package it.vali;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
        public static void main(String[] args) {


//        while (true) {
//            System.out.println("tere");
//        }
//    }

            // Programm mõtleb välja numbri
            // Programm küsib kasutajalt arva number ära
            // Senikaua kuini kasutaja arvab valesti, ütleb proovi uuesti.

            Scanner scanner = new Scanner(System.in);

            do {
                Random random = new Random();

                // random.nextInt(5) genereerib numbri 0st 4ni
                // int number = random.nextInt(5) + 1;  // +1 siis 1st 5ni

                // 80 -100
                // int number = random.nextInt (20) + 80;

                // Teine võimalus


                int number = ThreadLocalRandom.current().nextInt(1,6);

                System.out.println("Arva ära number 1st 5ni");
                int enteredNumber = Integer.parseInt(scanner.nextLine());

                while (enteredNumber != number) {
                    System.out.println("Proovi uuesti");
                    enteredNumber = Integer.parseInt(scanner.nextLine());
                }

                System.out.println("Õige!");
                System.out.println("Kas soovid veel mängida?");
            } while (!scanner.nextLine().toLowerCase().equals("ei"));
        }
}
