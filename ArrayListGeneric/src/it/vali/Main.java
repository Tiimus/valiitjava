package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// Generic arraylist on selline kollektsioon, kus objekti loomise hetkel peame määrama, mis tüüpi elemente see sisaldama hakkab

        // List list = new ArrayList(); - oleks tavaline arraylist
        List<Integer> numbers = new ArrayList<Integer>();

        numbers.add(10);
        numbers.add(8);
        numbers.add(Integer.valueOf("10"));

        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers);
        }

        List<String> words = new ArrayList<String>();

        words.add("tere");

    }
}
