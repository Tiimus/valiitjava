package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // kõik if blokid ongi tegelikult booleanid

        boolean isMinor = false;
	    // võib ka: boolean isGrownup = false;

	    System.out.println("Kui vana sa oled?");

        Scanner scanner = new Scanner(System.in);

        int age = Integer.parseInt(scanner.nextLine());

        if (age < 18) {
            isMinor = true;
        }

        if(isMinor) {  // ehk pikalt if(isMinor == true)
            System.out.println("Oled alaealine");
        }
        else {
            System.out.println("Oled täisealine");
        }

        int number = age;
        boolean numberIsGreaterThan3 = number > 3;
        if (number > 2 && number < 6 || number > 10 || number < 20 || number == 100) {

        }

        boolean a = number > 2;
        boolean b = number < 6;
        boolean c = number > 10;
        boolean d = number < 20;
        boolean e = number == 100;
        boolean f = a && b;

        if (a && b || c || d|| e) {

        }

        if (true); // vahel kui on vaja teha väidet True'ks, näiteks kui on vaja testida




        // küsi kasutajalt, kas ta on söönud hommikusööki

        boolean hasEaten = false;
        System.out.println("Kas sa hommikusööki sõid? Kirjuta jah/ei");
        String answer = scanner.nextLine();

        if(answer.equals("jah")) {
            hasEaten = true;
        }

        System.out.println("Kas sa lõunat sõid? Kirjuta jah/ei");
        answer = scanner.nextLine();

        if(answer.equals("jah")) {
                hasEaten = true;
            }

        System.out.println("Kas sa õhtusööki sõid? Kirjuta jah/ei");
        answer = scanner.nextLine();

        if(answer.equals("jah")) {
            hasEaten = true;
        }

        if (hasEaten) {
            System.out.println("Oled täna söönud");
        }


        // küsi kasutajalt, kas ta on söönud lõunat
        // küsi kasutajalt, kas ta on söönud õhtusööki

        // prindi ekraanile, kas kasutaja on täna söönud või mitte


    }
}
