package it.vali;

public class Main {

    public static void main(String[] args) {
	    // Abstract klass on hübriid liidesest ja klassist
        // Selles klassis saab defineerida nii meetodite struktuure (nagu liideses), aga saab ka defineerida meetodi koos sisuga
        // Abstraktsest klassist ei saa otse objekti luua, saab ainult pärineda (ei saa panna Car car = new Car();) (sama interface'iga ka)

        ApartmentKitchen kitchen = new ApartmentKitchen();
        kitchen.setHeight(100);
        kitchen.becomeDirty();
    }
}
