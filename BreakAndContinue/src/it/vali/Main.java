package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Küsime kasutajalt pin koodi, kui see on õige, siis ütleme tore.
        // Muuljuhul küsime uuesti.
        // Kokku küsime kolm korda

        Scanner scanner = new Scanner(System.in);

        String realPin = "5555";

        // Break hüppab tsüklist välja (if ei ole tsükkel!). Break'i saab kasutada ainult tsüklite sees (loopi ja switchi sees).


        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta Pin kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(realPin)) {
                System.out.println("Tore, õige pin kood!");
                break;
            }
        }

        // prindi välja 10 kuni 20 ja 40 kuni 60

        // continue jätab selle tsüklikorduse katki ja läheb järgmise korduse juurde

        for (int i = 10; i <= 60; i++) {
            if(i > 20 && i < 40) {
                continue;
            }
            System.out.println(i);
        }
    }
}
