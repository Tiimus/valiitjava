package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[5];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;
        numbers[2] = 0;
        numbers[3] = 14;
        numbers[4] = 7;

        // eemalda siit teine number - vaata Lauri näiteid

        // Tavalisesse arraylisti võin lisada ükskõik mis tüüpi elemente
        // aga elemente küsisdes sealt, pean ma teadma, mis tüüpi element kus täpselt asub
        // ning pean siis selleks tüübiks küsimisel ka cast'ima  (teisendama)
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;

        Random random = new Random();

        list.add(money);
        list.add(random);

//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));   // osakb välja printida, aga ei oska midagi nendega teha (ei cast'i)
//        }

        int a = (int)list.get(1);
        String word = (String) list.get(0);  // kui oleks 1 siis tuleks java.lang.ClassCastException: class java.lang.Integer cannot be cast to class java.lang.String

        int sum = 3 + (int)list.get(1);

//        String sentence = list.get(0) + " hommikust";  Stringe oskab teisendada
//
//        System.out.println(sentence);

        if(Integer.class.isInstance(a)) {
            System.out.println("a on int");
        }

        if(Integer.class.isInstance(list.get(1))) {
            System.out.println("listis indeksiga 1 on int");
        }

        if(String.class.isInstance(list.get(0))) {
            System.out.println("listis indeksiga 0 on string");
        }

        list.add(2, "head aega");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(2.3456);


        list.addAll(otherList);

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        if (list.contains(money)) {
            System.out.println("money asub listis");
        }
        System.out.printf("moeny asub listis indeksiga %d%n", list.indexOf(money));  // addisime head aega vahele, sellepärast on indeks 4
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(23));
        System.out.printf("23 asub listis indeksiga %d%n", list.indexOf(44));  // kui ei leia, siis tagastab -1 :)

        list.remove(money);  // saab objekti nimega eemaldada

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        list.remove(5); // saab ka indexiga eemaldada

        System.out.println(list.size());

        list.set(0, "tore"); // asendab tere tore'ga, set asendab, add lisab juurde, saab ka inti stringiga asendada > kui 0 asemel näiteks 1

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
