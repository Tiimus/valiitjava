package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);  // Laseb meil rea kaupa lugeda

            // readLine loeb iga kord välja kutsudes järgmise rea


            // vaata do { lisamist Lauri failist

            String line = bufferedReader.readLine();  //esimene rida

//            line = bufferedReader.readLine();  // teine rida
//            System.out.println(line);

            // kui readLine avastab, et järgmist rida tegelikult ei ole, siis tagastab see meetod null
            while (line != null) {
                line = bufferedReader.readLine();
                System.out.println(line);
            }


            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
