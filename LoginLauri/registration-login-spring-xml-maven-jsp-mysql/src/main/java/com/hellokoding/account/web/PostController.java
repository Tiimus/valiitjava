package com.hellokoding.account.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hellokoding.account.model.Post;
import com.hellokoding.account.repository.PostDao;

public class PostController {
	@Autowired  
    PostDao dao;//will inject dao from xml file  
      
    /*It displays a form to input data, here "command" is a reserved request attribute 
     *which is used to display object data into form 
     */  
	// N�itab postide lisamise v�lja
    @RequestMapping("/postsaddform")  
    public String showform(Model m){  
    	m.addAttribute("command", new Post());
    	return "postsaddform"; 
    } 
    /*It saves object into database. The @ModelAttribute puts request data 
     *  into model object. You need to mention RequestMethod.POST method  
     *  because default request is GET*/  
    // Posti lisamine
    @RequestMapping(value="/postsaddform",method = RequestMethod.POST)  
    public String enterPost(@ModelAttribute("post") Post post){  
        dao.enterPost(post);  
        return "redirect:/viewposts";//will redirect to viewposts request mapping  
    }  
    /* It provides list of employees in model object */  
    // List postitustest
    @RequestMapping("/viewposts")  
    public String viewposts(Model m){  
        List<Post> list=dao.getPosts();  
        m.addAttribute("list",list);
        return "viewposts";  
    }  
    
    /* It displays object data into form for the given id.  
     * The @PathVariable puts URL data into variable.
    @RequestMapping(value="/editemp/{id}")  
    public String edit(@PathVariable int id, Model m){  
        Emp emp=dao.getEmpById(id);  
        m.addAttribute("command",emp);
        return "empeditform";  
    }  */
    
    /* It updates model object.  
    // Hiljem �mber teha moderaatori jaoks Edit funktsiooniks
    @RequestMapping(value="/editsave",method = RequestMethod.POST)  
    public String editsave(@ModelAttribute("emp") Emp emp){  
        dao.update(emp);  
        return "redirect:/viewemp";  
    }  
    /* It deletes record for the given id in URL and redirects to /viewemp  
    @RequestMapping(value="/deleteemp/{id}",method = RequestMethod.GET)  
    public String delete(@PathVariable int id){  
        dao.delete(id);  
        return "redirect:/viewemp";  
    }   */
}
