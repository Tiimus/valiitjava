package com.hellokoding.account.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Post;

public class PostDao {
JdbcTemplate template;  
String word = "";
public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
// �he uue postituse loomine
public int enterPost(Post p){  
    String sql="insert into Posts(title,text,time_added) values('"+p.getTitle()+"','"+p.getText()+"',"+p.getTimeAdded()+")";  
    return template.update(sql);  
} 
// K�igi (vanade) postide n�itamine
public String viewPosts() {
	String sql="select * from Posts order by time_added asc";
	return sql;
}
// Valitud posti n�itamine
public String readSinglePost(Post p) {
	String sql="SELECT * from Posts where id="+p.getId()+"";
	return sql;
}
// �he postituse muutmine (moderaatori jaoks)
public int modifyPost(Post p){  
    String sql="update Posts set title='"+p.getTitle()+"', text='"+p.getText()+"' where id="+p.getId()+"";  
    return template.update(sql);  
}  
// Viie v�rskeima postituse n�itamine
public String getPostsByDatePublished(){  
    String sql="SELECT * FROM Posts ORDER BY timePublished DESC LIMIT 5";  
    return sql;  
}  
// Postide laikide j�rgi n�itamine
public String getPostsByLikes() {
	String sql="select * from Posts order by likes desc";
	return sql;
}
// Postide m�rks�nade j�rgi n�itamine
public String getPostsByWords(String word) {
	String sql="select * from Posts where title like %"+word+"% or text like %"+word+"% order by timePublished desc";
	return sql;
}
// Postituste nimekirja n�itamine
public List<Post> getPosts(){  
    return template.query("select * from Posts",new RowMapper<Post>(){  
        public Post mapRow(ResultSet rs, int row) throws SQLException {  
            Post p=new Post();  
            p.setId(rs.getInt(1));  
            p.setTitle(rs.getString(2));  
            p.setText(rs.getString(3));  
            p.setLikes(rs.getInt(4));  
            p.setTimeAdded(rs.getDate(5));
            return p;  
        }  

    });  
   }  
   }  