package com.hellokoding.account.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Post {
	private int id;  
	private String title;  
	private String text;  
	private int likes;  
	private Date timeAdded;  
	private Date timePublished;
	private boolean isPublished;
	
	
	DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss EEEEE");
	String curTime = dateFormat.format(new Date());
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public Date getTimeAdded() {
		return timeAdded;
	}
	// Annab postitusele lisamise aja
	public void setTimeAdded(Date curTime) {
		this.timeAdded = curTime;
	}
	public Date getTimePublished() {
		return timePublished;
	}
	// Annab postitusele avaldamise aja
	public void setTimePublished(Date curTime) {
		this.timePublished = curTime;
	}
	public boolean isPublished() {
		return isPublished;
	}
	public void setPublished(boolean isPublished) {
		this.isPublished = isPublished;
	}
}
