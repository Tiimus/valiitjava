package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String sentence = "Väljas on ilus ilm, vihma ei saja ja päike paistab";

        //Split tükeldab Stringi ette antud sümbolite kohalt ja tekitab sõnade massiivi
        // | tähendab regulaaravaldises või'd
        String[] words = sentence.split(" ja | |, ");  // regex - regulaaravaldis  (" ", 3 ); 3 on limit, ehk siis teeks kolmeks osaks

        for (int i = 0; i < words.length; i++) {
            System.out.println(words[i]);
        }

        String newSentence = String.join(" ", words);  // (mis ma tahan et neile sõnadele vahele pandaks, massiivi nimi)
        System.out.println(newSentence);

        newSentence = String.join("\n", words);  // %n'i kasutatakse ainult printf'is ja string.format'iga
        System.out.println(newSentence);
        // .csv -
        // Escaping  - kõik, mis algavad \'ga
        System.out.println("Juku ütles: \n \"Mulle meeldib suvi\"");  // \b - backspace; \t - tab; \" - prindib "


        // Küsi kasutajalt rida numbreid nii, et ta paneb need numbrid kirja ühele reale eraldades tühikuga.
        //Seejärel liidab need kõik numbrid kokku ja prindib vastuse
        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades tühikuga");
        String numbersText = scanner.nextLine();

        String[] numbers = numbersText.split(" ");

        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += Integer.parseInt(numbers[i]);   // sum = sum + Integer.parseInt(numbers[i]);
        }
        String joinedNumbers = String.join(", ", numbers);
        System.out.printf("Arvude %s summa on %d%n", joinedNumbers, sum);
    }
}
