package it.vali;

public class Main {

    public static void main(String[] args) {
	    float a = 123.34f; // f lisatakse, et teha vahet floatil ja double'il
	    double b = 123.4343;

		// arvude 123.34f ja 123.4343 summa on
		// arvude 123.34f ja 123.4343 jagatis on
		// arvude 123.34f ja 123.4343 korrutis on

		System.out.printf("Arvude %f ja %f summa on %f%n", a, b, (a + b) );
		System.out.printf("Arvude %f ja %f jagatis on %f%n", a, b, (a / b) );
		System.out.printf("Arvude %f ja %f korrutis on %f%n%n", a, b, (a * b) ); //2 x %n - nage kaks korda enter

		System.out.printf("Arvude %e ja %e summa on %e%n", a, b, (a + b) );
		System.out.printf("Arvude %f ja %f summa on %f%n", a, b, (a + b) );
		System.out.printf("Arvude %g ja %g summa on %g%n", a, b, (a + b) );


		// printige jagatis; tulemuses E12 on astmes 12 ehk 12 nulli on taga
		float c = 131111f;
		float e = 23111111f;

		System.out.println(c / e);

		double f = 131111;
		double g = 23111111;

		System.out.println(f / g);

		System.out.println(c * e);
		System.out.println(f * g);

    }
}
