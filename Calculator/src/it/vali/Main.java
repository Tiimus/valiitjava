package it.vali;

public class Main {

    public static void main(String[] args) {
        int sum = sum(4, 5);    // int sum (variable) = Sum (meetod)
        System.out.printf("Arvude 4 ja 5 summa on %d", sum);

        System.out.println();

        // pikem versioon
        int[] numbers = new int[3];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = -2;

        sum = sum(numbers);
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        //lühem versioon
        sum = sum(new int[]{1, 2, -2,});
        System.out.printf("Massiivi arvude summa on %d%n", sum);

        System.out.println();

        // pikem versioon:
        int[] reversed = reverseNumbers(numbers);

        printNumbers(reversed);

        // lühem versioon:
        printNumbers(reverseNumbers(numbers));

        // Massiive saab ka niimoodi välja kirjutada: printNumbers(new int[] {1, 2, 3, 4, 5});

        //     printNumbers(reverseNumbers(new int[] {1, 2, 3, 4, 5}));


        System.out.println();

        printNumbers(convertToIntArray(new String[]{"2", "-12", "1"}));

        System.out.println();

        String sentence = arrayToSentence(new String[]{"Homme", "on", "ilus", "ilm"});
        System.out.println(sentence);

        System.out.println();

        // Niimoodi saab koodi loetavamaks teha, koma juurest uus rida
        System.out.printf("Sõnade massiiv liidetuna sõnadeks on: %s%n",
                arrayToSentenceWithDivider(" ja ", new String[]{"Homme", "on", "ilus", "ilm"}));

        double average = calculateAverage(new int[]{2, 3, 7});
        System.out.println(average);

        System.out.println();

//        double percentage = calculatePercentage(20, 100);
//        System.out.println(percentage);

        // Kui tahame kasutada %märki printf'i sees, siis kasutame topelt % ehk %%
        int a = 20;
        int b = 100;
        System.out.printf("Arv %d on arvust %d %.0f protsenti %n", a, b, calculatePercentage(a, b));

        System.out.println();

        System.out.printf("Ringi ümbermõõt on %.2f", calculateCircleCircumference(7));

        System.out.println();

        // Kuidas println'i sees saab valida mida printida
        int age = 18;
        System.out.println(age > 21 ? "Tohid kasiinosse siseneda" : "Ei tohi siseneda");

    }

    // Meetod, mis liidab 2 arvu kokku ja tagastab nende summa
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subtract
    static int subtract(int a, int b) {
        int subtract = a - b;
        return subtract;
    }

    // multiply
    static int multiply(int a, int b) {   // võib teha ka lihtsalt return a * b; siis rida int multiply = a * b; jääb ära
        int multiply = a * b;
        return multiply;
    }

    // divide
    static double divide(int a, int b) {   //kui ma tahan kogu blokki tab võrra tagasi, siis highlight ja siis Shift+Tab
        return (double) a / b;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi ja liidab elemendid kokku ning tagastab summa

    static int sum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    // Meetod, mis võtab parameetriks täisarvude massiivi, pöörab tagurpidi ja tagastab selle
    // 1 2 3 4 5
    // 5 4 3 2 1

    static int[] reverseNumbers(int[] numbers) {

        int[] reversedNumbers = new int[numbers.length];

        for (int i = 0, j = numbers.length - 1; i < numbers.length; i++, j--) {   // pikkuseks võib olla ka reversedNumbers.length sest mõlemad [] on ühepikkused
            reversedNumbers[i] = numbers[j];
        }
        return reversedNumbers;
    }

    // Meetod, mis prindib välja täisarvude massiivi elemendid

    static void printNumbers(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
    }

    // Meetod, mis võtab parameetriks stringi massiivi (eeldusel, et tegelikult seal massiivis on numbrid stringidena)
    // ja teisendab numbrite massiiviks ning tagastab selle

    static int[] convertToIntArray(String[] numbersAsText) {
        int[] numbers = new int[numbersAsText.length];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Integer.parseInt(numbersAsText[i]);
        }
        return numbers;
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning tagastab lause, kus iga stringi vahel on tühik.

    static String arrayToSentence(String[] inputArray) {
        //String sentence = String.join(" ", inputArray);
        //return sentence
        return String.join(" ", inputArray);
    }

    // Meetod, mis võtab parameetriks stringi massiivi ning teine parameeter on sõnade eraldaja. Tagastada lause.
    // Vaata eelmist ülesannet, lihtsalt tühiku asemel saad ise valida, millega sõnu eraldab

    static String arrayToSentenceWithDivider(String divider, String[] inputArray) {

        String sentence = String.join(divider, inputArray);

        return sentence;
    }

    // Meetod, mis leiab numbrite massiivist keskmise, ning tagastab selle

    static double calculateAverage(int[] numbers) {
        return sum(numbers) / numbers.length;

//        int sum = 0;
//        for (int i = 0; i < numbers.length; i++) {
//            sum += numbers[i];
//        }
//        int average = sum / numbers.length;
//        return average;
    }

    // Meetod, mis arvutab mitu % moodustab esimene arv teisest.

    static double calculatePercentage(int a, int b) {
        double percentage = (double)a / b * 100;
        return percentage;
    }

    // Meetod, mis arvutab ringi ümbermõõdu raadiuse järgi

    static double calculateCircleCircumference(double radius) {
        double circumference = 2 * Math.PI * radius;
        return circumference;
    }
}