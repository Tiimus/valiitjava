package it.vali;

public class Main {

    public static void main(String[] args) {

        Cat angora = new Cat();
        angora.setName("Miisu");
        angora.setAge(10);
        angora.setBreed("Angora");
        angora.setWeight(2.34);

        angora.printInfo();
        angora.eat();

        Cat persian = new Cat();

        persian.setName("Liisu");
        persian.setAge(5);
        persian.setBreed("Persian");
        persian.setWeight(3.56);

        persian.printInfo();
        persian.eat();

        Dog tax = new Dog();
        tax.setName("Muki");
        tax.setAge(2);
        tax.setBreed("Tax");
        tax.setWeight(4.55);

        tax.printInfo();
        tax.eat();

        persian.catchMouse();
        tax.playWithCat(persian);

        // Lisa pärinevusahelast puuduvad klassid
        // Igale klassile lisa 1 muutuja ja 1 meetod, mis on ainult sellele klassile omane

    }
}
