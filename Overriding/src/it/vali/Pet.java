package it.vali;

public class Pet extends DomesticAnimal {

    private int numberOfOwners;
    private String nameOfOwner;

    public int getNumberOfOwners() {
        return numberOfOwners;
    }

    public void setNumberOfOwners(int numberOfOwners) {
        this.numberOfOwners = numberOfOwners;
    }

    public String getNameOfOwner() {
        return nameOfOwner;
    }

    public void setNameOfOwner(String nameOfOwner) {
        this.nameOfOwner = nameOfOwner;
    }

    public void isGettingPetted() {
        System.out.println("Tehakse pai");
    }

    @Override
    public void printInfo() {
        super.printInfo();
        System.out.printf("Omaniku nimi: %d%n", nameOfOwner);
    }


}
