package it.vali;

public class Main {

    public static void main(String[] args) {
	// Method Overriding ehk ülekirjutamine
        // (tähendab seda, et kuskil klassis, millest antud klass pärineb, oleva meetodi sisu kirjutatakse pärinevas klassis üle)
        // päritava klassi meetodi sisu kirjutatake pärinevas klassis üle

        Dog buldog = new Dog();
        buldog.eat();

        Cat siam = new Cat();
        siam.eat();

        siam.printInfo();

        // Kirjuta koera getAge üle nii, et kui koeral vanus on 0, siis näitaks ikka 1
        System.out.println(buldog.getAge());
        System.out.println(siam.getAge());
        buldog.getAge();

        System.out.println();

        buldog.setAge(10);

        buldog.getName();
        buldog.printInfo();

        buldog.setName("Rex");
        buldog.printInfo();

        // Metsloomadel printInfo võiks kirjutada Nimi: metsloomal pole nime

        WildAnimal cheetah = new WildAnimal();
        cheetah.printInfo();

    }
}
