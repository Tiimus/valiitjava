package it.vali;

public class DomesticAnimal extends Animal {

    public String getOwnersName() {
        return ownersName;
    }

    public void setOwnersName(String ownersName) {
        this.ownersName = ownersName;
    }

    private String ownersName;

    public void askingForFood() {
        System.out.println("Küsib süüa");
    }
}
