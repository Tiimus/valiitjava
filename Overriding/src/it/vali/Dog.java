package it.vali;

public class Dog extends Pet {  // Kui ma tahan klassinime või mõne variable'i jne muuta, siis Refactor > Rename

    private boolean hasTail = true;

    public boolean isHasTail() {
        return hasTail;
    }

    public void setHasTail(boolean hasTail) {
        this.hasTail = hasTail;
    }

    public void playWithCat(Cat cat) {
        System.out.printf("Mängin kassiga %s%n", cat.getName());
    }

    @Override
    public void eat() {
        System.out.println("Närin konti");
    }

    @Override
    public int getAge() {
        int age = super.getAge();
        if (age == 0) {
            age = 1;
            System.out.printf("Koera vanus on %d%n", age);
            return 1;
        }
        return age;
    }

}