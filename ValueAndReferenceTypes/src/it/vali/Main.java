package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        // Kui panna üks primitiiv (value type) tüüpi muutuja võrduma teise muutujaga
        // siis tegelikult tehakse arvuti mällu uus muutuja ja väärtus kopeeritakse sinna

        // Sama juhtub ka primitiiv (value type) tüüpi muutuja kaasa andmisel meetodi parameetriks

        int a = 3;
        int b = a;
        a = 7;

        System.out.println(b);

        increment(b);
        System.out.println(b);

        // Reference tüüpi on array'd ja objektid
        // Value tüüpi int, double, string, boolean etc


        // Kui panna Reference type (viit-tüüpi - viitavad ühte kohta) võrduma teise muutujaga
        // siis tegelikult jääb mälus ikkagi alles ainult üks muutuja
        // lihtsalt teine muutuja hakkab viitama samale kohale mälus (samale muutujale)
        // Meil on 2 muutujat, aga tegelikult on täpselt sama object

        // Sama juhtub ka reference type muutuja kaasa andmisel meetodi parameetriks

        int[] numbers = new int[] {-5};   //kui on tühi siis on üks number

        int [] secondNumbers = numbers;
        numbers[0] = 3;

        System.out.println(secondNumbers[0]);

        Point pointA = new Point();
        pointA.x = 10;
        pointA.y = 3;

        Point pointB = pointA;
        pointB.x = 7;

        System.out.println(pointA.x);

        List<Point> points = new ArrayList<Point>();
        points.add(pointA);
        points.add(pointB);

        pointA.y = -4;

        System.out.println(points.get(0).y);
        System.out.println(points.get(1).y);
        System.out.println(pointB.y);

        // mitte static - pead looma objekti, et välja kutsuda
        // static - sa ei pea objekti looma, et meetodit välja kutsuda, piisab lihtsalt klassist
        // Instance meetod vs static meetod - instance sellele ühele juhumile kohane (sellele objektile)

        pointA.printNotStatic();

        Point.printStatic();

        Point pointC = new Point();
        pointC.x = 12;
        pointC.y = 20;
        increment(pointC);
        System.out.println(pointC.x);

        System.out.println();

        int[] thirdNumbers = new int[] {1, 2, 3};
        increment(thirdNumbers);
        System.out.println(thirdNumbers[0]);

        Point pointD = new Point();
        pointD.x = 5;
        pointD.x = 6;
        pointD.increment();


    }

    public static void increment (int a) {
        a++;
        System.out.printf("a on nüüd %d%n", a);
    }

    // Suurendame x ja y koordinaate 1 võrra
    // see meetod võiks olla ka point klassi all, aga siis peaks main koodis seda meetodit välja kutsuma, et algab Point. (kuna siis on see Pointi meetod) (Point.increment(Point point))
    public static void increment(Point point) {
        point.x++;
        point.y++;
    }

    // Suurendame kõiki elemente 1 võrra
    // static - sa ei pea objekti looma, et meetodit välja kutsuda
    public static void increment(int [] numbers) {

        for (int i = 0; i < numbers.length; i++) {
            numbers[i]++;
        }
    }
}
