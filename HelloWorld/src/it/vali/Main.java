package it.vali;
// public - klass, meetod või muutuja on avalikult nähtav/ligipääsetav (kui seda ei ole, siis kaks eri java faili ei saa üksteisega suhelda)
// class - javas üksus, üldiselt on ka eraldi fail, mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld - klassi nimi, mis on ka faili nimi
// static - meetodi ees tähendab, et seda meetodit saab väljakutsuda ilma klassist objekti loomata
// void - meetod ei tagasta midagi
// 	meetodile on võimalik kaasa anda parameetrid, mis pannakse sulgude sisse, eraldades komaga.
// String [] - tähistab stringi massiivi
// args - massiivi nimi, sisaldab käsurealt kaasa pandud parameetreid
// System.out.println - java meetod, millega saab välja printida teksti. See, mis kirjatatakse sulgudesse, prinditakse välja ja tehakse reavahetus
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");
	// write your code here
    }
}
