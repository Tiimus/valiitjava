import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main (String[] args) {
        Map<String, String> map = new HashMap<String, String>();   // LinkedHashMapi näide allpool

        // Sõnaraamat
        // key > value
        // Maja > House
        // Puu > Tree

        map.put("Maja", "House");
        map.put("Isa", "Dad");
        map.put("Puu", "Tree");
        map.put("Sinine", "Blue");

        // Map järjekorda ei salvesta

        for (Map.Entry<String, String>  entry: map.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());  // entry saab ainult mapidega kasutada
        }

        // Oletame, et tahame teada, mis on inglise keeles Puu

        System.out.println();

        String translation = map.get("Puu");  // get(key) => tulemuseks (translation) value
        System.out.println(translation);

        System.out.println();

        Map<String, String> idNumberName = new HashMap<String, String>();
        idNumberName.put("48405120252", "Taimi");
        idNumberName.put("37805120457", "Kalle");

        // kui kasutada put sama key lisamisel, kirjutatakse value üle (ehk siis kirjutatake välja viimane)
        // Saab olla ainult üks key/key on unikaalne

        System.out.println(idNumberName.get("48405120252"));

        System.out.println();

        idNumberName.remove("48405120252");
        System.out.println(idNumberName.get("48405120252"));

        System.out.println();

        // Eesti > Est
        // Eesti > +372

        // Loe lauses üle kõik erinevad tähed ning prindi välja iga tähe järel, mitu tükki teda selles lauses oli

        // Char on selline tüüp, kus saab hoida üksikut sümbolit
        char symbolA = 'a';
        char newLine = '\n';

        String sentence = "elas metsas mutionu";
        // e 2
        // l 1

        Map<Character, Integer> letterCounts = new HashMap<Character, Integer>();

        char[] characters = sentence.toCharArray();
        for (int i = 0; i < characters.length; i++) {
            if (letterCounts.containsKey(characters[i])) {
                letterCounts.put(characters[i], letterCounts.get(characters[i]) + 1);
            } else {
                letterCounts.put(characters[i], 1);
            }
        }
        System.out.println(letterCounts);

        // Map.Entry<Character, Integer> - on klass, mis hoiab endas ühte rida map-is
        // ehk siis ühte key-value paari
        for (Map.Entry<Character, Integer> entry : letterCounts.entrySet()) {
            System.out.printf("Tähte %s esines %d korda%n", entry.getKey(), entry.getValue());
        }


        // LINKEDHASHMAP - kui tahame, et säiliks lisamise järjekord

        Map<String, String> linkedMap = new LinkedHashMap<String, String>();   // võib panna ka LinkedHashMap, siis tuleb selles järjekorras kui sa sisestasid

        // Sõnaraamat
        // key > value
        // Maja > House
        // Puu > Tree

        linkedMap.put("Maja", "House");
        linkedMap.put("Isa", "Dad");
        linkedMap.put("Puu", "Tree");
        linkedMap.put("Sinine", "Blue");

        for (Map.Entry<String, String>  entry: linkedMap.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());  // entry saab ainult mapidega kasutada
        }
    }
}
