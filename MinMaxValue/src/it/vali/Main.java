package it.vali;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[] { -2, 4, 10, 7, 0, 6 };  // Kuidas on võimalik ka teistmoodi massiivi välja kirjutada. Index on massiivi järjekorranumber näiteks 0 = -2; 1 = 4 jne

        // Leiame maksimum numbri massiivist

        int max = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] > max) {
                max = numbers[i];
            }
        }
        System.out.println(max);

        int min = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < min) {
                min = numbers[i];
            }
        }
        System.out.println(min);

        // Leia suurim paaritu arv
        // Mitmes number see on

//        int maxOdd = numbers[0];  // max = Integer.max_value; -
//        int counter;

        max = Integer.MIN_VALUE;   // paneme kõige väiksema Inti, mis on võimalik, sest siis on kindel, et see millega me võrdleme on suurem.
        int position = 1;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
            }
        }

        // suurim paaritu number on 11 ja ta on järjekorras number 3
        System.out.printf("Suurim paaritu number on %d ja ta on järjekorras number %d%n", max, position);


        // Kui paarituid arve üldse ei ole, prindi, et paaritud arvud puuduvad"

        max = Integer.MIN_VALUE;
        position = 1;
        boolean oddNumbersFound = false;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
                oddNumbersFound = true;
            }
        }
        if (oddNumbersFound) {
            System.out.printf("Suurim paaritu number on %d ja ta on järjekorras number %d%n", max, position);
        }

        else {
            System.out.println("Paaritud arvud puuduvad");
        }

        // Teine versioon

        max = Integer.MIN_VALUE;
        position = -1;   // -1 on tavaline, kui tahetakse vaadata, kas leitakse või mitte

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] % 2 != 0 && numbers[i] > max) {
                max = numbers[i];
                position = i + 1;
            }
        }


        if (position != -1) {
        System.out.printf("Suurim paaritu number on %d ja ta on järjekorras number %d", max, position);

         } else {
            System.out.println("Paaritud arvud puuduvad");
        }
    }
}
