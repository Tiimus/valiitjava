package it.vali;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class Main {

    static String pin;

    public static void main(String[] args) {
	// Sisesta PIN kood
    // Vali toiming a) Sularaha sissemakse, b) Sularaha väljamakse c) Kontojääk d) Muuda PIN koodi

        // Valiti b:
        // Vali summa a) 5 eur b) 10 eur c) 20 eur d) Muu summa

        // Valiti d:
        // Sisesta PIN kood:
        // 1234
        // Sisesta uus pin kood:
        // 2345
        // Sisesta uus PIN uuesti
        // 2345
        // Pin kood muudetud

        // hoiame pin koodi failis pin.txt ja kontojääki balance.txt

        Scanner scanner = new Scanner(System.in);  // Scanner - Klass; scanner - objekt;

        int balance = loadBalance();
        String pin = loadPin();

        if (!validatePin()) {
            System.out.println("Kaart konfiskeeritud");
            return;   // viskab meetodist välja, ehk siis siin juhul viskab main meetodist välja (nagu break, mis viskab tsüklist välja)
        }

        // Pini muutmine
        // pin = "2345";
        savePin(pin);

        // Lisa konto väljavõtte funktsionaalsus
        // System.out.println(currentDateTimeToString() + "Sularaha väljavõtt -100 eur");

        //Date date = new Date();
        //Calendar cal = Calendar.getInstance();

        // raha väljavõtmine:
        // int amount = 0;
        // balance -= amount;
        // saveBalance(balance);


        // boolean correctPin = false;

        int pinTries = 0;
        String inputPin = "";

        do {
            System.out.println("Palun sisesta Pin kood:");
            inputPin = scanner.nextLine();
            if (inputPin.equals(pin)) {
                System.out.println("Õige Pin!");
                break;
            } else {
                pinTries++;
                if (pinTries == 3) {
                    System.out.println("Pin koodi sisestamine ebaõnnestus, palun pöörduge panka.");
                }
                else {
                    System.out.println("Vale Pin, palun proovi uuesti");
                }
            }
        } while (pinTries != 3);

        if (inputPin.equals(pin)){
            System.out.printf("Mida sa sooviksid teha? %nPalun vali toiming: %n" +
                    "a) Sularaha sissemakse %nb) Sularaha väljamakse %nc) Kontojääk %nd) Muuda PIN koodi %ne) Konto väljavõte %n");

            String answer = scanner.nextLine();

            switch (answer) {
                case "A":
                case "a":
                    System.out.println("Palun sisesta summa, mida sooviksid sisse maksta:");
                    int addedMoney = Integer.parseInt(scanner.nextLine());
                    balance += addedMoney;
                    saveBalance(balance);
                    saveTransaction(addedMoney, "Deposit");
                    System.out.printf("%d eurot on lisatud teie kontole", addedMoney);
                    break;
                case "B":
                case "b":
                    System.out.printf("Kui palju te sooviksite raha välja võtta? %n" +
                        "Palun vali summa: %na) 5 eur %nb) 10 eur %nc) 20 eur %nd) Muu summa%n");
                    String withdrawalAnswer = scanner.nextLine();
                    switch (withdrawalAnswer) {
                        case "A":
                        case "a":
                            System.out.println("Palun võtke raha");
                            int amount = 5;
                            balance -= 5;
                            if (balance > amount) {
                                System.out.println("Pole piisavalt vahendeid");
                            }
                            saveBalance(balance);
                            break;
                        case "B":
                        case "b":
                            System.out.println("Palun võtke raha");
                            balance -= 10;
                            saveBalance(balance);
                            break;
                        case "C":
                        case "c":
                            System.out.println("Palun võtke raha");
                            balance -= 20;
                            saveBalance(balance);
                            break;
                        case "D":
                        case "d":
                            System.out.println("Palun sisestage summa, mida te soovite välja võtta");
                            int withdrawalAmount = Integer.parseInt(scanner.nextLine());
                            balance -= withdrawalAmount;
                            saveBalance(balance);
                            System.out.println("Palun võtke raha");
                            break;
                        default:
                            System.out.println("Selline toiming puudub, palun valige uuesti");
                            break;
                    }
                    break;
                case "C":
                case "c":
                    System.out.printf("Teie kontojääk on %d eurot", loadBalance());
                    break;
                case "D":
                case "d":
                    System.out.println("Palun sisestage oma Pin kood:");
                    inputPin = scanner.nextLine();
                    String newPin = "";
                    String newPinAgain = "";
                    do {
                        if (inputPin.equals(pin)) {
                            System.out.println("Palun sisestage uus Pin kood:");
                            newPin = scanner.nextLine();
                            System.out.println("Palun sisestage uus Pin kood uuesti:");
                            newPinAgain = scanner.nextLine();
                            if (newPin.equals(newPinAgain)) {
                                savePin(newPinAgain);
                                System.out.println("Pin kood muudetud");
                            } else {
                                System.out.println("Uus Pin kood valesti sisestatud.");
                            }
                        }
                    } while (!(newPin.equals(newPinAgain)));
                    break;
                case "E":
                case "e":
                    printTransactions();
                    break;
                default:
                    System.out.println("Selline toiming puudub, palun valige uuesti");
                    break;
            }


        }


    }

    // toimub pin kirjutamine pin.txt faili
    static void savePin(String pin) {
        try {
            FileWriter fileWriter = new FileWriter("pin.txt");
            fileWriter.write(pin + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Pin koodi salvestamine ebaõnnestus");
        }
    }

    // toimub pin välja lugemine pin.txt failist
    // toimub shadowing
    // Shadowing refers to the practice in Java programming of using two variables with the same name within scopes that overlap.
    // When you do that, the variable with the higher-level scope is hidden because the variable with lower-level scope overrides it.
    // The higher-level variable is then “shadowed.”
    static String loadPin() {
        String pin = null;   // võib ka "" panna
        try {
            FileReader fileReader = new FileReader("pin.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            pin = bufferedReader.readLine();
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Pin koodi lugemine ebaõnnestus");
        }
        return pin;

    }


    static void saveBalance(int balance) {
        // Toimub balance kirjutamine balance.txt.faili
        try {
            FileWriter fileWriter = new FileWriter("balance.txt");
            fileWriter.write(balance + System.lineSeparator());
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi salvestamine ebaõnnestus");
        }
    }

    static int loadBalance() {
        // Toimub balance välja lugemine balance.txt failist

        int balance = 0;
        try {
            FileReader fileReader = new FileReader("balance.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            balance = Integer.parseInt(bufferedReader.readLine());
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Kontojäägi laadimine ebaõnnestus");
        }
        return balance;
    }
        // Lisa kontoväljavõtte funktsionaalsus
        // System.out.println(currentDateTimeToString() + "Sularaha väljavõtt -100 eur");
        // Konto väljavõte:
        // Sularaha sissemakse +100 eur
        // Sularaha väljamakse - 200 eur

    static void saveAccountActivity() {
        try {
            FileWriter fileWriter = new FileWriter ("account.txt");

            fileWriter.append("Tere\r\n");

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Konto tehingute salvestamine ebaõnnestus");
        }
    }

        // Date:
    static String currentDateTimeToString() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    static boolean validatePin() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Palun sisesta Pin kood");

            String enteredPin = scanner.nextLine();

            if(enteredPin.equals(pin)) {
                System.out.println("Õige pin!");
                return true;
            }
        }
        return false;
    }
    static void saveTransaction(int amount, String transaction) {
        try {
            FileWriter fileWriter = new FileWriter("account.txt", true);

            if (transaction.equals("Deposit")) {

                fileWriter.append(String.format("%s Raha sissemaks +%d%n", currentDateTimeToString(), amount));

            } else {
                fileWriter.append(String.format("%s Raha väljamakse -%d%n", currentDateTimeToString(), amount));
            }

        } catch (IOException e) {
            System.out.println("Tehingu salvestamine ebaõnnestus");
        }

    }

    static void printTransactions(){
        try {
            FileReader fileReader = new FileReader("account.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);  // Laseb meil rea kaupa lugeda

            String line = bufferedReader.readLine();

            while (line != null) {
                line = bufferedReader.readLine();
                System.out.println(line);
            }

            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

