package it.vali;

public class Main {

    public static void main(String[] args) {
	    Monitor firstMonitor = new Monitor();
        Monitor secondMonitor = new Monitor();

        firstMonitor.manufacturer = "Philips";
        firstMonitor.color = Color.WHITE;     // kui enum siis pärast = märki suure tähega, siis saab pärast punkti panemist valida enumi valikuid
        firstMonitor.diagonal = 27;
        firstMonitor.screenType = ScreenType.AMOLED;

        secondMonitor.manufacturer = "LG";
        secondMonitor.color = Color.BLACK;
        secondMonitor.diagonal = 24;
        secondMonitor.screenType = ScreenType.LCD;

        System.out.println(firstMonitor.manufacturer);
        System.out.println(secondMonitor.screenType);

        System.out.println();

        firstMonitor.color = Color.BLACK;  // muudad värvi (deklareerid uuesti)

        int[] numbers = new int[2];

        Monitor[] monitors = new Monitor[4];


        // Lisa massiivi kolmas monitor

        Monitor thirdMonitor = new Monitor();

        thirdMonitor.manufacturer = "Samsung";
        thirdMonitor.color = Color.GREY;
        thirdMonitor.diagonal = 32;
        thirdMonitor.screenType = ScreenType.OLED;

        // Prindi välja kõigi monitoride tootja, mille diagonaal on suurem kui 25 tolli

        monitors[0] = firstMonitor;
        monitors[1] = secondMonitor;
        monitors[2] = thirdMonitor;

        monitors[3] = new Monitor();
        monitors[3].manufacturer = "Sony";
        monitors[3].color = Color.GREY;
        monitors[3].diagonal = 21;
        monitors[3].screenType = ScreenType.LCD;

        for (int i = 0; i < monitors.length; i++) {
            if(monitors[i].diagonal > 25) {
                System.out.println(monitors[i].manufacturer);
            }

        }

        System.out.println();

        // Leia monitori värv kõige suuremal monitoril

        // vale???
        double max = monitors[0].diagonal;
        for (int i = 0; i < monitors.length; i++) {
            if(monitors[i].diagonal > max) {
                max = monitors[i].diagonal;
                System.out.println(monitors[i].color);
            }
        }

        System.out.println();

        // Lauri variant

        Monitor maxSizeMonitor = monitors[0];

        for (int i = 0; i < monitors.length; i++) {
            if(monitors[i].diagonal > maxSizeMonitor.diagonal) {
                maxSizeMonitor = monitors[i];
            }
        }

        System.out.println(maxSizeMonitor.color);
        System.out.println(maxSizeMonitor.manufacturer);
        System.out.println(maxSizeMonitor.screenType);

        // Tegime Main.javasse meetodi nende printimiseks

        maxSizeMonitor.printInfo();
        firstMonitor.printInfo();

        // Tegime maini diagonaltoCm meetodi

        System.out.printf("Monitori diagonaal cm-des on %.2f%n", maxSizeMonitor.diagonalToCm());

    }
}
