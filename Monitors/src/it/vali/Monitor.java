package it.vali;

// Enum on tüüp, kus saab defineerida erinevaid lõplikke valikuid
// Tegelikult salvestatakse enum alati int-ina.
enum Color {
    BLACK, WHITE, GREY,
}

enum ScreenType {
    LCD, TFT, OLED, AMOLED
}

public class Monitor {
    String manufacturer;
    double diagonal;  // tollides "
    Color color;
    ScreenType screenType;

    // klassi meetoditel ei tule static ette


    void printInfo() {
        System.out.println();
        System.out.println("Monitori info: ");
        System.out.printf("Tootja: %s%n", manufacturer);
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", screenType);
        System.out.println();
    }

    // Tee meetod, mis tagastab ekraani diagonaali cm-des

    double diagonalToCm() {
        return diagonal * 2.54;
    }

}
