package it.vali;

public class Cow extends FarmAnimal {

    public boolean isHasPattern() {
        return hasPattern;
    }

    public void setHasPattern(boolean hasPattern) {
        this.hasPattern = hasPattern;
    }

    private boolean hasPattern;

    public void givesMilk() {
        System.out.println("Annan piima");
    }
}
