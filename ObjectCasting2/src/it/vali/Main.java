package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int a = 100;
        short b = (short) a;
        double c = a; // suuremast väiksemasse saab ilma castimata
        a = (int) c;

        // Iga kassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
        Cat cat = (Cat)animal;

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        Pig pig = new Pig();

        animals.add(dog);
        animals.add(pig);
        animals.add(new Cow());  // saab otse luua objekti ja lisada samal ajal, kui nii teed ei saa otse objekti poole pöörduda
        animals.add(new Pet());
        ((Pet)animals.get(animals.size() - 1)).setNameOfOwner("Juhan");
        animals.add(new WildAnimal());

        animals.get(animals.indexOf(dog)).setName("Peeter");  // get tahab sulgudesse Indexit
        animals.get(animals.size() - 1).setName("Jaan"); // Paneb viimasele lisatud Listi objektile nimeks Jaan



        // Kutsu kõikide loomade printInfo välja

        for (Animal animalInList: animals) {  // for(mis ma tahan, mis tüüpi elemendid on ja teen nime: mis listi ta läbi käib)
            animalInList.printInfo();
            System.out.println();
        }

        // Reflection in Java - saab küsida, näiteks mis subklassid on Animal klassil.

        Animal secondAnimal = new Dog();  // jäävad järgi ainult Animali meetodid, sest ma loon selle koera animali sisse, mitte eraldi Dog dog = new Dog();

        ((Dog)secondAnimal).setHasTail(true);  // teisendamine kehtib kõigele, mis tast paremal on ehk siis tuleb panna secondAnimali ümber sulud panema

        secondAnimal.printInfo();
    }

}
