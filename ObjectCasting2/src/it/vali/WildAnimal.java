package it.vali;

public class WildAnimal extends Animal {

    private boolean livesInForest;


    public boolean isLivesInForest() {
        return livesInForest;
    }

    public void setLivesInForest(boolean livesInForest) {
        this.livesInForest = livesInForest;
    }

    public void findsHome() {
        System.out.println("Leidsin endale kodu");
    }

    @Override
    public String getName() {
        return "Metsloomal pole nime";
    }
}
