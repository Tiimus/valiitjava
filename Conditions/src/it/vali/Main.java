package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    System.out.println("Sisesta number");

        Scanner scanner = new Scanner(System.in);
        int a = Integer.parseInt(scanner.nextLine());

        // Type-safe language - alati kui ma muutuja loon, ma panen ka, mis tüüpi see muutuaja on. Näiteks Phytonis seda ei ole

        if(a == 3) {
            System.out.printf("Arv %d on võrdne kolmega%n", a);
        }
        if (a < 5) {
            System.out.printf("Arv %d on väiksem viiest%n", a);
        }
        if (a != 4) {
            System.out.printf("Arv %d ei võrdu neljaga%n", a);
        }
        if (a > 2) {
            System.out.printf("Arv %d on suurem kahest%n", a);
        }
        if (a >= 7) {
            System.out.printf("Arv %d on suurem või võrdne seitsmega%n", a);
        }
        if (!(a >= 7)) {   //kogu tingimus vastupidiseks, pane sulgudesse ja lisa hüüumärk
            System.out.printf("Arv %d ei ole suurem või võrdne seitsmega%n", a);
        }
        // Arv on 2 ja 8 vahel
        if (a > 2 && a < 8) {
            System.out.printf("Arv %d on kahe ja kaheksa vahel%n", a);
        }
        // Arv on väiksem kui 2 või arv on suurem kui 8
        if (a < 2 || a > 8) {
            System.out.printf("Arv %d on väiksem kui kaks või suurem kui 8%n", a);
        }
        // Arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui kümme
        if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10) {  //&& on prioriteetsem kui ||, nii et sulgusid ei ole tegelikult vaja (aga võib panna)
            System.out.printf("Arv %d Arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui kümme%n", a);
        }
        // Arv ei ole 4 ja 6 vahel, aga on 5 ja 8 vahel või arv on negatiivne aga pole suurem kui -14
        if ( a != 5 && (a > 5 && a < 8) || a < 0 && !(a > -14)) {
            System.out.printf("Arv %d Arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui kümme%n", a);
        }
    }
}
