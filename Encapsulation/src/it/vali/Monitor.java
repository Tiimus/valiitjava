package it.vali;

// Enum on tüüp, kus saab defineerida erinevaid lõplike valikuid
// Tegelikult salvestatakse enum alati int-ina.
enum Color {
    BLACK, WHITE, GREY,
}

enum ScreenType {
    LCD, TFT, OLED, AMOLED
}


// private = need parameetrid ei ole väljapoole nähtavad
// Getter ja Setter - highlight näiteks color, siis code > Generate > getter and setter > vali mida tahad tekitada

public class Monitor {
    private String manufacturer;
    private double diagonal;  // tollides "
    private Color color;
    private ScreenType screenType;


    // kui ainult getYear siis ei saagi väärtust panna
    public int getYear() {
        return year;
    }

    private int year = 2000;


//    public String getManufacturer() {
//        return manufacturer;
//    }

    // Ära luba seadistada monitori tootjaks, mille tootja on Huawei
    // Kui keegi soovib seda tootjat monitori tootjaks panna, pannaks hoopis tootjaks tekst "Tootja puudub"

    // Keela ka tühja tootjanime lisamine "", null
    public void setManufacturer(String manufacturer) {
        if (manufacturer == null || manufacturer.equals("Huawei") || manufacturer.equals("") ) {  //null peab esimene olema
            System.out.println("tootja puudub");
            this.manufacturer = "Tootja puudub";
        } else {
            this.manufacturer = manufacturer;
        }
    }

    public double getDiagonal() {
        if(diagonal == 0) {
            System.out.println("Diagonaal on seadistamata");
        }
        return diagonal;
    }

    public void setDiagonal(double diagonal) {
        // this tähistab seda konkreetset objekti (Monitor)
        // if'iga saab panna piiranguid
        if(diagonal < 0) {
            System.out.println("Diagonaal ei saa olla negatiivne");
        } else if (diagonal > 100) {
            System.out.println("Diagonaal ei saa olla suurem kui 100");
        } else {
            this.diagonal = diagonal;
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color){
        this.color = color;
    }

    // kui ekraani tüüp on seadistamata (null), siis tagasta tüübiks LCD
    public ScreenType getScreenType() {
        if(screenType == null) {
            return ScreenType.LCD;
        }
        return screenType;
    }

    public void setScreenType(ScreenType screenType) {
        this.screenType = screenType;
    }


    // klassi meetoditel ei tule static ette

    // kui publicut ei oleks siis oleks see by default
    public void printInfo() {
        System.out.println();
        System.out.println("Monitori info: ");
        System.out.printf("Tootja: %s%n", manufacturer);  // siin ei ole vahet kas on private või public, sest see on sama klassi sees
        System.out.printf("Diagonaal: %.1f%n", diagonal);
        System.out.printf("Värv: %s%n", color);
        System.out.printf("Ekraani tüüp: %s%n", getScreenType());
        System.out.printf("Aasta: %d%n", year);
        System.out.println();
    }

    // Tee meetod, mis tagastab ekraani diagonaali cm-des

    public double diagonalToCm() {
        return diagonal * 2.54;
    }

}
