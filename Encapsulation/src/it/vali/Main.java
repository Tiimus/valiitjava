package it.vali;

public class Main {

    public static void main(String[] args) {

//        Monitor a = null;
//        a.printInfo(); // ühtegi meetodit ei saa välja kutsuda, kui väärtus on null, tuleb java.lang.NullPointerException

        // int vaikeväärtus 0
        // boolean vaikeväärtus false
        // double vaikeväärtus 0.0
        // float 0.0f
        // String vaikeväärtus null
        // objektide (Monitor, FileWriter) vaikeväärtus null
        // int[] arvud; null


        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-1000);
        System.out.println(firstMonitor.getDiagonal());

        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());

        System.out.println(firstMonitor.getYear());

        // Println käsk on juba meetodis sees
        firstMonitor.setManufacturer("Huawei");
        firstMonitor.setManufacturer("");
        firstMonitor.printInfo();
    }
}
