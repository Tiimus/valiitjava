package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int a = 100;
        short b = (short) a;
        double c = a; // suuremast väiksemasse saab ilma castimatta
        a = (int) c;

        // Iga kassi võib võtta kui looma
        // implicit casting
        Animal animal = new Cat();

        // Iga loom ei ole kass
        // explicit casting
        Cat cat = (Cat) new Animal();

        List<Animal> animals = new ArrayList<Animal>();

        Dog dog = new Dog();
        Pig pig = new Pig();

        animals.add(dog);
        animals.add(pig);
        animals.add(new Cow());  // saab otse luua objekti ja lisada samal ajal
        animals.add(new Pet());

        // Kutsu kõikide loomade printInfo välja

    }
}
