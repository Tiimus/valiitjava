﻿-- See on lihtne hello world teksti päring, mis tagastab ühe rea ja ühe veeru
-- (veerul puudub pealkiri). Selles veerus ja reas saab olema tekst Hello World
/* saab ka pikemat kommentaari kirjutada 
samamoodi nagu javas */

SELECT 'Hello World'; 

-- Täisarvu saab küsida ilma '' märkideta
SELECT 3;
-- palju analoogilist javaga
SELECT 3 * 4;
SELECT 3 / 4.0;

SELECT "raud" + "tee"; -- ei tööta PostgreSQL'is. MSSQL'is töötab - Microsoft SQL

SELECT CONCAT ('raud', 'tee'); -- õige variant
SELECT CONCAT ('raud', 'tee','.', 2, 0, 0, 4); -- saab ka muid asju liita

--Kui tahan erinevaid veerge, siis panen väärtustele koma vahele
SELECT 'Peeter', 'Paat', 23, 75.45, 'Blond' 

-- AS märksõnaga saab anda antud veerule nime
SELECT 
	'Peeter' AS eesnimi, 
	'Paat' AS perekonnanimi, 
	23 AS vanus, 
	75.45 AS kaal, 
	'Blond' AS juuksevärv
	NOW() AS kuupäev;  -- lisab kuupäeva
	
SELECT NOW(); -- tagastab pareguse kuupäeva ja kellaaja mingis vaikimini formaadis

-- Kui tahan konkreetset osa sellest, näiteks aastat või kuud 
SELECT date_part('year', NOW());

-- Kui tahan mingi kuupäeva osa ise etteantud kuupäevast, võid DATE'i ka kasutada
SELECT date_part('month', TIMESTAMP '2019-01-01');

-- Kui vaja kellaajast minuteid
SELECT date_part('minutes', TIME '10:45');	

-- Kuupäeva formaatimine Eesti kuupäeva formaati
SELECT to_char(NOW(), 'HH24:mi:ss DD.MM.YYYY'); 

-- interval laseb lisada või eemaldada mingit ajaühikut
-- Timestamp üks päev tagasi praegusest
SELECT now() + interval '1 day ago'  -- saab panna months, years, etc

--Üks päev juurde praegusest
SELECT now() + interval '1 day'
SELECT now() + interval '1 day 30 minutes'

-- kuueteiskümnendik süsteem ehk hex

-- tabeli loomine
CREATE TABLE student (
	id serial PRIMARY KEY,
	-- serial tähendab, et tüübiks on int, mis hakkab ühe võtta suurenema
	-- primary key, tähendab, et see on unikaalne väli tabelis
	first_name varchar(64) NOT NULL, -- ei tohi tühi olla
	last_name varchar(64) NOT NULL,
	height int NULL, -- tohib tühi olla
	weight numeric(5, 2) NULL,  -- 5 digits that includes 2 decimal points
	birthday date NULL
);
	
-- Tabelist kõikide ridade kõikide veergude küsimine
-- * tähendab, et anna kõik veerud
SELECT * FROM student;	

-- annab eesnimed
SELECT first_name FROM student;

-- arvutasime vanuse ja lisasime selle eraldi veergu
SELECT
	first_name AS eesnimi,
	last_name AS perekonnanimi,
	date_part('year', now()) - date_part('year', birthday) AS vanus
FROM 
	student;

-- 
SELECT CONCAT(first_name, ' ', last_name) AS täisnimi FROM student;	
-- Ees- ja perekonnanime vahel sünniaasta
SELECT CONCAT(first_name, ' ', date_part('year', birthday), ' ', last_name) AS täisnimi FROM student;

-- kui tahan otsida/filtreerida mingi tingimuse järgi - annab kõik inimesed, kelle pikkus on 180
SELECT 
	* 
FROM 
	student
WHERE 
	height = 180;
	
--	kõigi kelle eesnimi on Peeter
SELECT 
	* 
FROM 
	student
WHERE 
	first_name = 'Peeter';	

-- AND sama mis &&	
SELECT 
	* 
FROM 
	student
WHERE 
	first_name = 'Peeter' 
	AND last_name = 'Puujalg';	
	
-- 	OR sama mis ||
SELECT 
	* 
FROM 
	student
WHERE 
	first_name = 'Peeter' 
	OR height = 169;
	
-- küsi tabelist eesnime ja perekonnanime järgi mingi Peeter Tamm ja mingi Mari	Maasikas. 
SELECT 
	* 
FROM 
	student
WHERE 
	(first_name = 'Peeter' AND last_name = 'Pipar')
	OR 
	(first_name = 'Mari' AND last_name = 'Maasikas');
	
-- Anna mulle õpilased, kelle pikkus jääb 170-180 cm vahele.
SELECT 
	* 
FROM 
	student
WHERE 
	height >= 170 AND height <= 180;


-- Anna õpilased, kes on pikemad kui 170cm või lühemad kui 150 cm	

SELECT 
	* 
FROM 
	student
WHERE 
	height > 170 OR height < 150;
	
-- Anna õpilaste eesnimi ja pikkus, kelle sünnipäeva on jaanuaris
SELECT
	first_name, height
FROM
	student
WHERE 
	date_part('month', birthday) = 1;
	
	
-- Anna õpilased, kelle middle name on null (määramata) - IS 
SELECT 
	* 
FROM 
	student
WHERE 
	middle_name IS NULL;	
	
-- need, kelle middle name ei ole null - IS NOT
SELECT 
	* 
FROM 
	student
WHERE 
	middle_name IS NOT NULL;	
	
-- Anna õpilased, kelle pikkus ei ole 180 cm
SELECT 
	* 
FROM 
	student
WHERE 
	height != 180;	-- saab ka <> kasutada != asemel
	
-- Anna õpilased, kelle pikkus on 169, 145 või 180
SELECT 
	* 
FROM 
	student
WHERE 
	-- height = 169 OR height = 145 OR height = 180;
	height IN (169, 145, 180);

-- Anna õpilased, kelle eesnimi on Peeter, Mari või Kalle
SELECT 
	* 
FROM 
	student
WHERE 
	first_name IN ('Peeter', 'Kalle', 'Mari');

-- Anna õpilased, kelle eesnimi EI OLE Peeter, Mari või Kalle
SELECT 
	* 
FROM 
	student
WHERE 
	first_name NOT IN ('Peeter', 'Kalle', 'Mari');

-- Anna õpilased, kelle sünnikuupäev on kuu esimene, neljas või seitsmes päev
SELECT 
	* 
FROM 
	student
WHERE 
	date_part('day', birthday) IN (1, 4, 7);

-- kõik WHERE võrdlused jätavad välja NULL väärtusega read
-- Kõikidest võrdlemistest jäävad NULLid välja (kui on määramata)
SELECT 
	* 
FROM 
	student
WHERE 
	height > 0 OR height <= 0 OR height IS NULL; -- nüüd loeb ka NULLid välja

-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks (siis nullid lähevad kõige lõppu)
SELECT 
	* 
FROM 
	student
ORDER BY 
	height	
	
-- Anna mulle õpilased pikkuse järjekorras lühemast pikemaks ja siis kaalu järgi kui on sama pikkus
SELECT 
	* 
FROM 
	student
ORDER BY 
	height, weight

-- kui tahan tagurpidises järjekorras, lisandub sõna DESC (Descending)
-- Kui õiges järjekorras siis vaikimisi on ASC (Ascending)
-- kui on kaks tulemust, siis annab need suvalises järjekorras
-- saab ka ID järgi järjestada näiteks ID DESC, siis hakkab pihta viimasest, mis lisati
SELECT 
	* 
FROM 
	student
ORDER BY 
	first_name, ID DESC;	
	
-- Anna mulle vanuse järjekorras vanemast nooremaks õilaste pikkused, mis jäävad 160 ja 170 vahele. ORDER BY on alati viimane.
SELECT 
	height 
FROM 
	student
WHERE 
	height >= 160 AND height <= 170
ORDER BY 
	birthday;
	
-- Kui tahan otsida sõna seest mingit sõnaühendit
SELECT 
	* 
FROM 
	student
WHERE 
	first_name LIKE 'P%' -- eesnimi algab P'ga
	OR first_name LIKE '%ee%' -- ee on sõna sees
	OR first_name LIKE '%ari';	-- sõna lõpeb ari'ga
	
-- Kirjete sisestamine 	
INSERT INTO student 
	(first_name, last_name, height, weight, birthday, middle_name)
VALUES
	('Alar', 'Allikas', 181, 66.66, '1989-03-03', NULL),
	('Tiiu', 'Tihane', 183, 88.88, '1986-02-28', 'Tiia'),
	('Mari', 'Maasikas', 166, 56.55, '1984-05-12', 'Marleen');	
	
	-- projekt
INSERT INTO posts
	(title, text)
VALUES 
	('Kõige esimene', 'See on minu kõige esimene blogipostitus');
	
	
	
	
-- Täidab ainult need väljad, ülejäänud on NULL	
INSERT INTO student 
	(first_name, last_name)
VALUES
	('Artur', 'Alliksaar');

-- Tabelis muutmine: UPDATE lausega peab olema ETTEVAATLIK, alati peab kasutama WHERE'i lause lõpus, muidu muudab näiteks kõik pikkused ära
UPDATE 
	student
SET
	height = 172
	
-- 
UPDATE 
	student
SET
	height = 172
WHERE 
	ID = 4
-- või first_name = 'Kalle' AND last_name = 'Kalamees'

-- kui mitut asja muuta/lisada
UPDATE 
	student
SET
	height = 172, 
	weight = 100,
	middle_name = 'John',
	birthday = '1998-04-05'
WHERE 
	ID = 15
	
-- Muuda kõigi õpilaste pikkus ühevõrra suuremaks
UPDATE 
	student
SET
	height = height + 1;
	
-- Muuda hiljem kui 1999 sündinud õpilastel sünnipäev ühe päeva võrra suuremaks
UPDATE 
	student
SET
	birthday = birthday + interval '1 day'
WHERE
	date_part('year', birthday) > 1999;
	
-- Kustutamisel ka olla ETTEVAATLIK. ALATI kasuta WHERE'i
DELETE FROM 
	students
WHERE
	ID > 10;
	
-- CRUD operations - Create (Insert), Read (Select), Update, Delete

-- Loo uus tabel loan, millel on väljad: amount numeric(11,2), start_date, due_date, student_id
CREATE TABLE loan (
	id serial PRIMARY KEY,
	amount numeric(11,2) NOT NULL,
	start_date date NOT NULL, 
	end_date date NOT NULL,
	student_id int NOT NULL
);

-- Lisa nelja õpilase andmed uude tabelisse
-- kahele neist lisa veel üks laen
INSERT INTO loan
	(amount, start_date, end_date, student_id)
VALUES
	(500, '2017-12-12', '2020-04-05', 8),
	(2000, '2008-09-08', '2018-09-09', 5),
	(15000, NOW(), '2025-05-03', 4),
	(3000, '2018-05-03', '2020-08-07', 4),
	(1000, NOW(), '2026-09-07', 9),
	(2500, NOW(), '2022-06-05', 9)
	
--Anna mulle kõik õpilased koos oma laenudega
SELECT 
	*
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
	


SELECT 
	loan.*, student.* -- muudab veergude järjekorda
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
-- küsin kindlaid veerge
SELECT 
	student.first_name, 
	student.last_name,
	loan.amount,
	loan.end_date
FROM
	student
JOIN 
	loan
	ON student.id = loan.student_id
	
-- Anna mulle kõik õpilased koos oma laenudega
-- aga ainult kelle laen on suurem kui 500
-- koguse järgi suuremast väiksemaks

-- INNER JOIN on selline tabelite liitmine, kus liidetakse ainult need read 
-- kus on võrdsed student.id = loan.student.id
-- ehk need read, kus tabelite vahel on seos. Ülejäänud read ignoreeritakse
SELECT 
	student.first_name, 
	student.last_name,
	loan.amount
FROM
	student
JOIN -- INNER JOIN on vaikimisi join, INNER sõna võib ära jätta
	loan
	ON student.id = loan.student_id
WHERE 
	loan.amount > 500
ORDER BY 
	student.last_name, loan.amount DESC


-- Loo uus tabel loan_type, milles väljad name ja description. 
-- kui tabel loodud tuleb panna refresh vasakult tabelitele, siis ilmub uus tabel
CREATE TABLE loan_type (
	id serial PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(500) NULL
);

-- uue välja lisamine olemasolevale tabelile
ALTER TABLE loan
ADD COLUMN loan_type_id int;

-- http://www.postgresqltutorial.com/postgresql-alter-table/
-- tabeli kustutamine
DROP TABLE student;

-- lisasin laenutüübid
INSERT INTO loan_type
	(name, description)
VALUES 
	('Õppelaen', 'See on väga hea laen'),
	('SMS laen', 'See on väga väga halb laen'),
	('Väikelaen', 'See on kõrge intressiga laen'),
	('Kodulaen', 'See on madala intressiga laen')
	
-- kolme tabeli INNER JOIN
-- select tegelikult loetakse viimasena koodis
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
JOIN
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- INNER JOINI puhul järjekord ei ole oluline
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	loan_type AS lt
JOIN 
	loan AS l
	ON lt.id = l.loan_type_id
JOIN 
	student AS s
	ON s.id = l.student_id

-- LEFT JOIN võetakse joini eimesest (VASAKUST) tabelist kõik read
-- ning teises (paremas) tabelis näidatakse puuduvatel kohtadel NULL 
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s -- vasak, nagu hakkaks ülevaltpoolt vaatama
LEFT JOIN 
	loan AS l  -- parem 
	ON s.id = l.student_id
	
-- LEFT JOINI puhul on järjekord väga oluline
SELECT 
	s.first_name, s.last_name, l.amount, lt.name
FROM
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id
LEFT JOIN 
	loan_type AS lt
	ON lt.id = l.loan_type_id
	
-- annab kõik kombinatsioonid kahe tabeli vahel
SELECT 
	s.first_name, st.first_name
FROM
	student AS s
CROSS JOIN 
	student AS st
WHERE s.first_name != st.first_name

-- FULL OUTER JOIN annab sama mis LEFT JOIN + RIGHT JOIN ehk siis näitab mõlemad tabelid kokku
SELECT 
	s.first_name, s.last_name, l.amount
FROM
	student AS s
FULL OUTER JOIN 
	loan AS l
	ON s.id = l.student_id
	
	
-- Anna mulle kõigi kasutajate perekonnanimed, kes võtsid smslaenu
-- ja on võlgu üle 100 euro. Tulemused järjesta laenu võtja vanuse järgi
-- väiksemast suuremaks
SELECT 
	s.last_name
FROM
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
JOIN 
	loan_type AS lt
	ON lt.id = l.loan_type_id
WHERE 
	lt.name = 'SMS laen' 
	AND l.amount > 100
ORDER BY 
	s.birthday DESC

-- Aggregate functions:
-- Agregaatfunktsiooni selectis välja kutsudes kaob võimalus samas select lauses 
-- küsida mingit muud välja tabelist,
-- sest agregaatfunktsiooni tulemus on alati ainult üks number
-- ja seda ei saa kuidagi näidata koos väljadega, mida võib olla mitu rida 

-- saab pikkuste keskmise (NULLid jätab välja)
SELECT 
	AVG(height)
FROM
	student
	
-- palju on üks õpilane keskmiselt laenu võtnud
SELECT 
	AVG(loan.amount)
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
	
-- Arvestatakse ka neid õpilasi, kes ei ole laenu võtnud (siin koodis amount null) ehk muudab NULL väärtust
SELECT 
	AVG (COALESCE(loan.amount, 0))  -- nulli asemele saab ka teisi arve panna
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- saab vaadata, kus on failid
show data_directory;

-- veel agregaatfunktsioone
SELECT 
	ROUND(AVG(COALESCE(loan.amount, 0)), 0) AS "Keskmine laenusumma",
	MIN(loan.amount) AS "Minimaalne laenusumma",
	MAX(loan.amount) AS "Maksimaalne laenusumma",
	COUNT(*) AS "Kõikide ridade arv",
	COUNT(loan.amount) AS "Laenude arv", -- jäetakse välja read, kus loan.amount on NULL
	COUNT(student.height) AS "Mitmel õpilasel on pikkus"
FROM
	student
LEFT JOIN
	loan
	ON student.id = loan.student_id
	
-- Kasutades GROUP BY jäävad select päringu jaoks alles vaid need väljad, mis on GROUP BY's ära toodud (s.first_name, s.last_name)
-- Teisi välju saab ainult kasutada agregaatfunktsioonide sees 
SELECT
	s.first_name, s.last_name, SUM(l.amount)
FROM
	student AS s
JOIN
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name
	
-- Anna mulle laenude summad laenu tüüpide järgi

SELECT 
	lt.name, SUM(l.amount)
FROM 
	loan AS l
JOIN
	loan_type AS lt
	ON l.loan_type_id = lt.id 
GROUP BY 
	lt.name

	
-- Anna mulle laenude summad sünniaasta järgi

SELECT 
	date_part('year', s.birthday), SUM(l.amount)
FROM 
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday)

-- Laiendatud versioon

SELECT 
	date_part('year', s.birthday) AS sünniaasta, s.first_name, SUM(l.amount) -- veerude järjekord
FROM 
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	date_part('year', s.birthday), s.first_name
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- filtreerimisel saab kasutada ainult neid välju, mis on GROUP BY's
-- ja agregaatfunktsioone
HAVING 
	date_part('year', s.birthday) IS NOT NULL
	AND SUM(l.amount) > 1000 -- anna laenud, mille summa on suurem kui 1000
	AND COUNT(l.amount) = 2 -- anna ainult need read, kus on kaks laenu
ORDER BY 
	date_part('year', s.birthday) --järjestab sünniaasta järgi

-- näitab kõikide õpilaste laenude summasid
SELECT 
	s.first_name, s.last_name, date_part('year', s.birthday) AS sünniaasta, SUM(l.amount) -- veerude järjekord
FROM 
	student AS s
LEFT JOIN 
	loan AS l
	ON s.id = l.student_id
GROUP BY
	s.first_name, s.last_name, date_part('year', s.birthday)
-- HAVING on nagu WHERE, aga peale GROUP BY kasutamist
-- filtreerimisel saab kasutada ainult neid välju, mis on GROUP BY's
-- ja agregaatfunktsioone
HAVING 
	date_part('year', s.birthday) IS NOT NULL
ORDER BY 
	date_part('year', s.birthday) --järjestab sünniaasta järgi
	
-- Anna laenude summad grupeerituna õpilase ja laenu tüübi kaupa

SELECT 
	s.first_name, s.last_name, lt.name, SUM(l.amount)
FROM 
	student AS s
JOIN 
	loan AS l
	ON s.id = l.student_id
JOIN 
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY 
	s.first_name, s.last_name, lt.name
ORDER BY 
	first_name
	
-- Anna mitu laenu mingist tüübist on võetud ning mis on nende summad
-- sms laen 2 1000

SELECT 
	lt.name, COUNT(l.amount), SUM(l.amount)
FROM 
	loan AS l
JOIN 
	loan_type AS lt
	ON l.loan_type_id = lt.id
GROUP BY 
	lt.name
	
-- Mis aastal sündinud võtsid suurima summa laene
SELECT
	date_part('year', s.birthday), SUM(l.amount)
FROM 
	student AS s
JOIN 
	loan AS l
	ON l.student_id = s.id
GROUP BY
	date_part('year', s.birthday)
ORDER BY
	SUM(l.amount) DESC
LIMIT 1 -- ütleb, et anna ainult esimene rida

-- Anna õpilaste eesnime esitähe esinemise statistika
-- ehk siis mitme õpilase eesnimi algab mingi tähega

-- mis sõnast, mitmendast tähest ja mitu tähte
-- algab 1st -- SELECT SUBSTRING('kala', 1, 1)

SELECT 
	SUBSTRING(first_name, 1, 1),
	COUNT(SUBSTRING(first_name, 1, 1)) -- võib ka COUNT(first_name)
FROM
	student AS s
GROUP BY 
	SUBSTRING(first_name, 1, 1)
	
-- String functions http://www.postgresqltutorial.com/postgresql-string-functions/

SELECT FORMAT('Hello %s','PostgreSQL') -- kirjutab lihtsalt kokku

-- saan lauri
SELECT SUBSTRING ('lauri mattus', 1, POSITION(' ' in 'lauri mattus'))

-- saan mattus
-- siin see LENGTH('lauri mattus') võtab kuni stringi lõpuni või siis võib ka üldse ära jätta, 
-- lihtsalt panna algus ja annab vastuse kuni stringi lõpuni
SELECT SUBSTRING ('lauri mattus', POSITION(' ' in 'lauri mattus') + 1, LENGTH('lauri mattus')) 

-- NESTED QUERIES

-- subquery or inner query or Nested Query
-- Anna õpilased, kelle pikkus vastab keskmisele õpilaste pikkusele (muutsime ülesannet)
SELECT
	first_name, last_name, (SELECT weight FROM student WHERE id = 3)
FROM
	student
WHERE
	first_name IN 

(SELECT
	middle_name
FROM 
	student
WHERE 
	height = (SELECT ROUND(AVG(height)) FROM student))
	
-- Lisa kaks vanimat õpilast töötajate tabelisse (ei pea VALUES panema siin)
INSERT INTO employee
	(first_name, last_name, birthday, middle_name)

SELECT first_name, last_name, birthday, middle_name FROM student ORDER BY birthday LIMIT 2

-- Anna mulle kõik matemaatikas õppivad õpilased
-- anna mulle kõik õppeained, kus jaan õpib



-- teisendamine
SELECT CAST(ROUND(AVG(age)) AS UNSIGNED) FROM test.emp99




	
	
	


	


	
	