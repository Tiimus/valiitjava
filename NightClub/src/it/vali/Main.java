package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String savedFirstName = "Taimi";
        String savedLastName = "Tiimus";


        /* Kõigepealt küsitakse külastaja nime
         * kui nimi on listis, siis öeldake kasutajale
         * "Tere tulemast Lauri"
         * ja küsitakse külastaja vanust
         * kui kasutaja on alaealine, teavitatakse teda, et sorry sisse ei pääse
         * muul juhul siis öeldake tere tulemast klubisse*/

        /*
         kui kasutaja ei olnud listis küsitakse kasutajalt ka tema perekonnanime
        * kui perekonnanimi on listis, siis öeldakse tere tulemast eesnimi ja perekonnanimi
        * ja muul juhul öeldakse, et "ma ei tunne sind"
        */
        Scanner scanner = new Scanner(System.in);

        System.out.println("Mis su nimi on?");
        String guestFirstName = scanner.nextLine();

        if (savedFirstName.toLowerCase().equals(guestFirstName.toLowerCase())) {
            System.out.printf("Tere %s. Kui vana sa oled?%n", guestFirstName.toUpperCase());
            int age = Integer.parseInt(scanner.nextLine());
            if (age < 18) {
                System.out.println("Kahjuks ei pääse sisse");
            } else {
                System.out.println("Tere tulemast klubisse");
            }
        } else {
            System.out.println("Mis su perekonnanimi on?");
            String guestLastName = scanner.nextLine();
            if (guestLastName.toLowerCase().equals(savedLastName.toLowerCase())) {
                System.out.printf("Tere tulemast %s", savedFirstName + " " + savedLastName);
            } else {
                System.out.println("Ma ei tunne sind");
            }
        }
    }
}
