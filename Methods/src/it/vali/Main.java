package it.vali;

public class Main {

    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust.
    // Kui koodis on korduvaid koodi osasid, võiks mõelda, et äkki peaks nende kohta tegema eraldi meetodi

    public static void main(String[] args) {  //kui publicut ei ole, siis vaikimisi on private

        printHello();

        System.out.println();

        printHello(3);

        System.out.println();

        printText("Kuidas läheb?");

        System.out.println();

        printText("Hästi läheb", 2);

        System.out.println();

        printText(1985, "Finland");

        System.out.println();

        printText("tere", 1, true);
        printText("tere", 1, false);
    }

    // Lisame meetodi, mis prindib ekraanile Hello

    private static void printHello() {   // kõik mis on classi sees pääsevad meetodile ligi,
        // nii et järjekord siin ei ole oluline (kus meetod defineeritud on)
        System.out.println("Hello");
    }

    // Lisame meetodi, mis prindib Hello etteantud arv kordi

    static void printHello(int howManyTimes) {   // (parameetri tüüp, parameetri nimi)
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");
        }
    }

    // Lisame meetodi, mis prindib ette antud teksti välja
    // printText

    static void printText(String text) {

        System.out.println(text);
    }

    // Lisame meetodi, mis prindib etteantud teksti välja ette antud arv kordi

    static void printText(String text, int howManyTimes) {     //erinevad parameetrid eralda komaga; kui panna ette int ja siis String, siis saab ka teha sama nimega meetodi
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);
        }
    }

    static void printText(int year, String text) {
            System.out.printf("%d: %s%n", year, text);
        }

     // Method OVERLOADING - meil on mitu meetodit sama nimega, aga erinevate parameetrite kombinatsiooniga - Meetodi ülelaadimine

    // Lisame meetodi, mis prindib etteantud teksti välja ette antud arv kordi
    // Lisaks saab öelda, kas tahame teha tähed enne suurteks või mitte

    static void printText(String text, int howManyTimes, boolean toUpperCase) {     //erinevad parameetrid eralda komaga
        for (int i = 0; i < howManyTimes; i++) {
            if(toUpperCase) {
                System.out.println(text.toUpperCase());
            }
            else {
                System.out.println(text);
            }
        }
    }

}
