package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // Do while tsükkel on nagu while tsükkel, ainult et kontroll tehakse peale esimest kordust. ÜKs kordus tehakse alati ja siis kontrollitakse

//        System.out.println("Kas tahad jätkata? Jah/ei");
//        // jätkame seni, kuni kasutaja kirjutab "ei"
//
//        Scanner scanner = new Scanner(System.in);
//
//        String answer = scanner.nextLine();
//
//
//        while (!answer.equals("ei")) {
//            System.out.println("Kas tahad jätkata? Jah/ei");
//            answer = scanner.nextLine();
//        }

        // muutuja kehtib ainul oma loogsulgude sees)

        Scanner scanner = new Scanner(System.in);
        String answer;       //Iga muutuja, mille me deklareerime, kehtib ainult seda ümbritsevate {} sees

        do {
            System.out.println("Kas tahad jätkata? Jah/ei");
            answer = scanner.nextLine();
        }
        while (!answer.equals("ei"));
    }
}
