package it.vali;

import java.io.*;   // * tähendab, et kõik java.io paketid on lisatud
import java.nio.charset.Charset;

public class Main {

    public static void main(String[] args) {
	// Loe failist input.txt iga teine rida ning kirjuta need read faili output.txt

        try {

            // Notepad vaikimisi kasutab ANSI encodingut. Selleks, et FileReader oskaks seda korrektselt lugeda (täpitähti), peame talle ette ütlema, et loe seda ANSI encodingus. (Või siis input faili salvestama enne UTF-8'sse)
            // Cp1252 on javas ANSI encoding
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt", Charset.forName("Cp1252"));  // kui fail on juba enne salvestatud UTF-8's, siis ei pea ANSI'it lisama ehk siis see milles on salvestatud peab matchima sellega, mis on koodis.

            // Faili kirjutades on javas vaikeväärtus UTF-8
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\Documents\\output.txt");  // ehk siis ("c:\\users\\opilane\\Documents\\output.txt", Charset.forName("UTF-8"))

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = bufferedReader.readLine();
            int lineNumber = 1;

            while (line != null) {
                if (lineNumber % 2 == 1) {
                    fileWriter.write(line + System.lineSeparator());
                }
                line = bufferedReader.readLine();
                lineNumber++;
            }

            bufferedReader.close();  // mõistlik bufferReader enne kinni panna kui fileReader, sest ta on fileReaderi sees. Hakka peale seespoolt väljapoole.
            fileReader.close();
            fileWriter.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
