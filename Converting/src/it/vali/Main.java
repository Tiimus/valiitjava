package it.vali;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    // küsi kasutajalt 2 numbrit
        // prindi välja nende summa

        Scanner scanner = new Scanner(System.in);

        System.out.println("Sisesta esimene number");

        int a = Integer.parseInt(scanner.nextLine());  // ei kasuta nextLine'i sest nextLine nõuab alati Stringi (kasutaja võib sinna suvalist teksti kirjutada ka numbreid) Siin me küsime numbrit, ehk ei saa Stringi kasutada

        System.out.println("Sisesta teine number");

        int b = Integer.parseInt(scanner.nextLine());

        System.out.println("Arvude summa on " + (a + b));

        System.out.printf("Numbrite %d ja %d summa on %d%n", a, b, a + b);

        // Küsi nüüd 2 reaalarvu ja prindi nende jagatis

        System.out.println("Sisesta kolmas number");

        double c = Double.parseDouble(scanner.nextLine());  // ei kasuta nextLine'i, sest nextLine nõuab alati Stringi (kasutaja võib sinna suvalist teksti kirjutada ka numbreid) Siin me küsime numbrit, ehk ei saa Stringi kasutada

        System.out.println("Sisesta neljas number");

        double d = Double.parseDouble(scanner.nextLine());

        System.out.printf("Numbrite %.2f ja %.2f jagatis on %.2f%n", c, d, c / d); // %.2f - siis jätab ainult kaks komakohta

        // Kui tahan näha 0,56 asemel 0.56, siis ütlen eraldi ette, et kasuta USA local-i
        System.out.println(String.format(Locale.US,"%.2f", c / d));
    }
}
