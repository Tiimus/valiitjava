package it.vali;

import java.io.FileWriter;  // io - input output  // Field - väli
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        // Try plokis otsitakse/oodatakse Exceptioneid (Erind, Erand, Viga), midagi, mida ma saan lahendada
        try {

            // FileWriter on selline klass, mis tegeleb faili kirjutamisega.
            // Sellest klassist ojbjekti loomisel antakse talle ette faili asukoht
            // Faili asukoht võib olla ainult faili nimega kirjeldatud: output.txt - sel juhul kirjutatakse faili, mis asub samas kaustas, kus meie Main.class (.class fail on see, mis kompileerib)
            // või täispika asukohaga c:\\users\\opilane\\documents{{output.txt
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\Documents\\output.txt");

            fileWriter.write(String.format("Elas metsas Mutionu%n"));
            fileWriter.write("keset kuuski noori vanu" + System.lineSeparator());
            fileWriter.write("kadak-põõsa juure all\r\n");



            fileWriter.close();   // muidu operatsioonisüsteem ei tea, et sa oled lõpetanud. Kui ei lõpeta, siis võtab palju mälu või võimalik, et teised ei saa sinna midagi salvestada

        // Catch plokis püütakse kinni kindlat tüüpi Exception või kõik exceptionid, mis pärinevad antud Exceptionist
        } catch (IOException e) {
            // printStackTrace tähendab, et prinditakse välja meetodite välja kutsumise hierarhia/ajalugu (näitab Exceptioni ajalugu all aknas)

            // e.printStackTrace();   //kommenteerin välja, et lõppkasutaja ei näeks
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }
    }
}
