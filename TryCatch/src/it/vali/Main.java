package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int a = 0;
        try {
//            String word = null;
//            word.length();  // pärast seda viga hüppab kohe veateatesse, järgnevaid ridu ei loe
            int b = 4 / a;
        }

        // Kui on mitu catch plokki, siis otsib ta esimese ploki, mis oskab antud Exceptioni kinni püüda

        catch (ArithmeticException e) {   // e tähendab Exceptionit, mõnes keeles ka ex
            if(e.getMessage().equals("/ by zero")) {
                System.out.println("Nulli ei saa jagada");
            }
            else {
            System.out.println("Esines aritmeetiline viga");
            System.out.println(e.getMessage());
            }

        }

        // //RunTime Exception - viga, mida sa enne programmi käivitamist ei näe
        catch (RuntimeException e) {
            System.out.println("Reaalajas esinev viga");
        }

        // Exception on klass, millest kõik erinevad Exceptioni tüübid pärinevad.
        // Mis omakorda tähendab, et püüdes kinni selle üldise Exceptioni, püüame me kinni kõik Exceptionid
        catch (Exception e) {
            // e.printStackTrace();
            System.out.println("Esines viga.");
        }

        // Küsime kasutajalt numbri ja kui number ei ole korrektses formaadis, siis ütleme mingi veateate
        // Pärast küsi uuesti kui oli vale

        Scanner scanner = new Scanner(System.in);

        boolean correctNumber = false;

        do {
            System.out.println("Palun kirjuta number");
            try {
                int answer = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Number oli vigane");
            }
        } while (!correctNumber);

        // Loo täisarvude massiiv 5 täisarvuga ning ürita sinna lisada kuues täisarv.
        // Näita veateadet

        int[] numbers = new int[] {1, 2, 3, 4, 5};

       try {
           numbers[5] = 6;
       } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri");
       }

       // see osa ei ole siin ülesandes tegelikult vajalik
       catch (Exception e) {
           System.out.println("Juhtus mingi tundmatu viga");
       }
    }
}
