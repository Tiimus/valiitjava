package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Küsi kasutajalt kaks arvu, siis küsi, mis tehet ta soovib teha
        // a) liitmine, b) lahutamine c) korrutamine d) jagamine
        // Prindi kasutajale tehte vastus
        // kui tehe tehtud, küsi uuesti
        // kui paneb vale tähe, siis küsime uuesti, mis tehte ta teha tahab

        Scanner scanner = new Scanner(System.in);

        do {   // loope saab sisestada ka selekteerid sele osa, mis sa tahad tsükklisse panna ja paned Code > Surround with
            System.out.println("Palun sisesta üks arv");
            int a = Integer.parseInt(scanner.nextLine());
            System.out.println("Palun sisesta teine arv");
            int b = Integer.parseInt(scanner.nextLine());

            String answer;
            boolean wrongAnswer;

            do {
                System.out.println("Millist tehet sa sooviksid teha?");
                System.out.println("a) liitmine");
                System.out.println("b) lahutamine");
                System.out.println("c) korrutamine");
                System.out.println("d) jagamaine");

                answer = scanner.nextLine();
                wrongAnswer = false;

//                if (answer.equals("a")) {
//                    System.out.printf("%d pluss %d võrdub %d%n", a, b, sum(a, b));
//                } else if (answer.equals("b")) {
//                    System.out.printf("%d miinus %d võrdub %d%n", a, b, subtract(a, b));
//                } else if (answer.equals("c")) {
//                    System.out.printf("%d korda %d võrdub %d%n", a, b, multiply(a, b));
//                } else if (answer.equals("d")) {
//                    System.out.printf("%d jagatud %d võrdub %.2f%n", a, b, divide(a, b));
//                } else {
//                    System.out.println("Selline tehe puudub, palun vali uuesti");
//                    wrongAnswer = true;
//                }

                // Switch case konstruktsioon sobib kasutamiseks if ja else if asemel, siis kui if ja elsi if kontrollivad ühe ja sama muutuja väärtust

                switch (answer) {
                    case "A":   // kui breaki ei pane siis prindib kuni break'ini
                    case "a":
                        System.out.printf("%d pluss %d võrdub %d%n", a, b, sum(a, b));
                        break;
                    case "B":
                    case "b":
                        System.out.printf("%d miinus %d võrdub %d%n", a, b, subtract(a, b));
                        break;
                    case "C":
                    case "c":
                        System.out.printf("%d korda %d võrdub %d%n", a, b, multiply(a, b));
                        break;
                    case "D":
                    case "d":
                        System.out.printf("%d jagatud %d võrdub %.2f%n", a, b, divide(a, b));
                        break;
                    default:
                        System.out.println("Selline tehe puudub, palun vali uuesti");
                        wrongAnswer = true;
                        break;
                }

                //} while (!answer.equals("a") && !answer.equals("b") && !answer.equals("c") && !answer.equals("d"));
            } while (wrongAnswer);

            System.out.println("Kas soovid veel arvutada? jah/ei");

        } while (!scanner.nextLine().toLowerCase().equals("ei"));
        System.out.println("Olgu, mõnusat päeva!");
    }

    // add
    static int sum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    // subtract
    static int subtract(int a, int b) {
        int subtract = a - b;
        return subtract;
    }

    // multiply
    static int multiply(int a, int b) {
        return a * b;
    }

    // divide
    static double divide(int a, int b) {
        return (double) a / b;
    }
}
