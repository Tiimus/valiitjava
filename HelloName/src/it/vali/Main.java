package it.vali;

public class Main {

    public static void main(String[] args) {
        // Deklareerime/defineerime tekstitüüpi (String) muutuja (variable)
        // mille nimeks paneme name ja väärtuseks Taimi
	    String name = "Taimi";
	    System.out.println("Hello " + name);
	    String lastName = "Tiimus";
	    System.out.println("Hello " + name + " " + lastName);
    }
}
