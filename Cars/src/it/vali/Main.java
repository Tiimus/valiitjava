package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    Car bmw = new Car();

		System.out.println();

	    bmw.startEngine();
	    bmw.startEngine();
	    bmw.stopEngine();
	    bmw.stopEngine();

		System.out.println();

		bmw.accelerate(100);

		System.out.println();

		bmw.startEngine();
		bmw.accelerate(100);


		Car fiat = new Car();  // Car() - kutsutakse konstruktor välja
		Car mercedes = new Car();

		System.out.println();

		Car opel = new Car("Opel", "Vectra", 1999, 205);
		opel.startEngine();
		opel.accelerate(210);

		System.out.println();

		Person person = new Person ("Taimi", "Tiimus", Gender.FEMALE, 34 );


		// Loo mõni auto objekt, kellel on siis määratud kasutaja ja omanik

		Car citroen = new Car("Citroen", "C4", 2005, 200);

		citroen.startEngine();
		citroen.park();

		System.out.println();

		citroen.setDriver(person);
		citroen.setOwner(person);

		// Prindi välja auto omaniku vanus

		System.out.printf("Sõiduki %s omaniku vanus on %d%n", citroen.getMake(), citroen.getOwner().getAge());

		Person secondPerson = new Person("Lauri", "Mattus", Gender.MALE, 35 );

		citroen.setMaxNumberPassengers(4);

		citroen.addPassenger(secondPerson);
		citroen.addPassenger(person);

		System.out.printf("Autos reisija on %s%n", secondPerson.getFirstName());

		citroen.showPassengers();

		Person juku = new Person("Juku", "juurikas", Gender.MALE, 23);
		Person malle = new Person("malle", "maasikas", Gender.FEMALE, 34);

		System.out.println(juku);


    }

}
