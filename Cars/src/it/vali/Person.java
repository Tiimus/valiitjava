package it.vali;

enum Gender {
    FEMALE, MALE, NOTSPECIFIED
}

public class Person {
    private String firstName;
    private String lastName;
    private Gender gender;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Kui klassil ei ole defineeritud konstruktorit, siis tegelikult tehakse nähtamatu parameetrita konstruktor, mille sisu on tühi

    // Kui klassile ise lisada mingi konstruktor, siis see nähtamatu parameetrita konstruktor kustutatakse
    // Sellest klassist saab siis teha objekti ainult selle uue konstruktoriga
    // (kui tahan uuesti ilma parameetriteta konstruktorit, siis pean ise uue tegema - overloading)
    public Person(String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Eesnimi on: %s, perekonnanimi on %s, vanus on %d", firstName, lastName, age);
    }
}
