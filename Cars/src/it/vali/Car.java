package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {
    GAS, PETROL, DIESEL, HYBRID, ELECTRIC
}

public class Car {
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    private String make;
    private String model;
    private int year;
    private Fuel fuel;
    private boolean isUsed;

    private boolean isEngineRunning;  //vaikimisi false
    private int speed;
    private int maxSpeed;

    public int getMaxNumberPassengers() {
        return maxNumberPassengers;
    }

    public void setMaxNumberPassengers(int maxNumberPassengers) {
        this.maxNumberPassengers = maxNumberPassengers;
    }

    private int maxNumberPassengers;

    // Lisa autole parameetrid driver ja owner (tüübist Person) ja tee nendele siis get ja set meetodid
    private Person owner;

    public Person getOwner() {

        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getDriver() {
        return driver;
    }

    public void setDriver(Person driver) {
        this.driver = driver;
    }

    private Person driver;

    // Lisa autole võimalus hoida reisijaid

    private List<Person> passengers = new ArrayList<Person>();

    // Lisa meetodid reisijate lisamiseks ja eemaldamiseks autost
    // Kontrolli ka, et ei lisaks rohkem reisijaid kui mahub

    public void addPassenger (Person passengerName) {
        if(maxNumberPassengers > passengers.size()) {
            if(passengers.contains(passengerName)) {
                System.out.printf("Autos on juba reisija %s%n", passengerName.getFirstName());
                return;
            }
            passengers.add(passengerName);
            System.out.printf("Autosse lisati reisija %s%n", passengerName.getFirstName());
        }
    }

    public void removePassenger (Person passengerName) {
        if(passengers.indexOf(passengerName) != -1 ){
            passengers.remove(passengerName);
            System.out.printf("Autost eemaldati reisija %s%n", passengerName.getFirstName());
        } else {
            System.out.println("Autos sellist reisijat pole");
        }
    }

    public void showPassengers() {

        // for each loop
        // Iga elemendi kohta listis passengers tekita objekt passenger
        // 1. kordus Person passenger on esimene reisija
        // 2. kordus Person passanger on teine reisija

        System.out.println("Autos on järgnevad reisijad: ");
        for(Person passenger : passengers) {
            System.out.println(passenger.getFirstName());
        }

        // for loobi variant
//        for (int i = 0; i < passengers.size(); i++) {
//            System.out.println(passengers.get(i).getFirstName());
//        }
    }


    // Konstruktor (constructor)
    // on eriline meetod, mis käivitatakse klassist objekti loomisel (kui objekt luuakse)
    // Näiteks saad siia panna midagi kohe faili kirjutama
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;  // false'i pole mõtet panna, sest see on default
        fuel = Fuel.PETROL;

    }

    // Constructor overloading
    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;                    // siin on this, sest ta ei tee vahet kas on parameetri make või klassi make
        this.model = model;
        this.year = year;
        this.maxSpeed = maxSpeed;
    }

    public Car(String make, String model, int year, Fuel fuel, boolean isUsed, int maxSpeed) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.fuel = fuel;
        this.isUsed = isUsed;
        this.maxSpeed = maxSpeed;
    }

    public void startEngine () {
        if (!isEngineRunning) {
            isEngineRunning = true;
            System.out.println("Mootor käivitus");
        } else {
            System.out.println("Mootor juba töötab");
        }
    }

    public void stopEngine() {
        if (isEngineRunning) {
            isEngineRunning = false;
            System.out.println("Mootor seiskus");
        } else {
            System.out.println("Mootor ei töötanudki");
        }
    }

    public void accelerate(int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa kiirendada");
        } else if (targetSpeed > maxSpeed){
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada madalamale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n", speed);
        }
    }

    // slowDown (int targetSpeed)

    public void slowDown (int targetSpeed) {
        if(!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");
        } else if (targetSpeed > speed) {
            System.out.println("Auto ei saa aeglustuda kõrgemale kiirusele");
        } else {
            speed = targetSpeed;
            System.out.printf("Auto aeglustas kuni kiiruseni %d%n", targetSpeed);
        }
    }
    // park(), mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid)

    public void park() {
            slowDown(0);
            if (isEngineRunning) {
                stopEngine();
            }
            System.out.println("Auto on pargitud");
        }

}
