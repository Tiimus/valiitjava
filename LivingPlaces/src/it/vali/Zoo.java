package it.vali;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zoo implements LivingPlace {

    private List<Animal> animals = new ArrayList<Animal>();

    // Siin hoitakse infot kui palju meil loomi farmis on
    private Map<String, Integer> animalCounts = new HashMap<String, Integer>();

    // Palju meil igat looma farmi mahub
    private Map<String, Integer> maxAnimalCounts = new HashMap<String, Integer>();

    public Zoo() {
        maxAnimalCounts.put("Lion", 2);
        maxAnimalCounts.put("Fox", 3);

    }

    @Override
    public void addAnimal (Animal animal) {

        // Kas animal on tüübist FarmAnimal või pärineb sellest tüübist
        if(Pet.class.isInstance(animal)) {
            System.out.println("Lemmikloomi Zoos ei ole");
            return;
        }


        String animalType = animal.getClass().getSimpleName();

        if(!maxAnimalCounts.containsKey(animalType)) {
            System.out.println("Loomaias selliseid loomi ei ole");
            return;
        }

        if(animalCounts.containsKey(animalType)) {
            int animalCount = animalCounts.get(animalType);
            int maxAnimalCount = maxAnimalCounts.get(animalType);
            if(animalCount >= maxAnimalCount) {
                System.out.println("Loomaaias on sellele loomale kõik kohad juba täis");
                return;
            }
            animalCounts.put(animalType, animalCounts.get(animalType) + 1);
            // Sellist looma veel ei ole farmis
            // Kindlasti sellele loomale kohta on
        } else {

            animalCounts.put(animalType, 1);
        }

        animals.add(animal);

        System.out.printf("Loomaaeda lisati loom %s%n", animalType);

    }

    @Override
    public void printAnimalCounts() {

    }

    @Override
    public void removeAnimal(String animalType) {

        // Teen muutuja mis ma tahan, et iga element listist oleks
        // (esmalt tüüp, mis ta on - FarmAnimal ja siis panen ise nime sellel muutujale - farmAnimal) : list, mida ma tahan läbi käia
        // FarmAnimal animal hakkab olema järjest esimene loom, siis teine loom, kolmas ja seni kuni loomi on

        boolean animalFound = false;

        for ( Animal animal : animals) {
            if(animal.getClass().getSimpleName().equals(animalType)) {
                animals.remove(animal);
                System.out.printf("Eemaldati loom %s%n", animalType);

                // Kui see oli viimane loom, siis eemalda see rida animalCounts mapist
                // muuljuhul vähenda animalCounts mapis seda kogust

                if(animalCounts.get(animalType) == 1) {
                    animalCounts.remove(animalType);
                } else {
                    animalCounts.put(animalType, animalCounts.get(animalType) - 1);
                }
                animalFound = true;
                break;
            }
        }
        if(!animalFound) {
            System.out.println("Antud loom puudub");
        }

//        for (int i = 0; i < animals.size(); i++) {
//            if(animals.get(i).getClass().getSimpleName().equals(animalType)) {
//                animals.remove(animals.get(i));
//            }
//        }

        // Täienda meetodit nii, et kui ei leitud sellest tüübist looma prindi "Farmis selline loom puudub"
    }
}
