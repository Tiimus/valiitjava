package it.vali;

public class CarnivoreAnimal extends WildAnimal {

    public String getAnimalForDinner() {
        return animalForDinner;
    }

    public void setAnimalForDinner(String animalForDinner) {
        this.animalForDinner = animalForDinner;
    }

    private String animalForDinner;

    public void catchingDinner() {
        System.out.println("Püüan õhtusööki");
    }
}
