package it.vali;

public class HerbivoreAnimal extends WildAnimal {

    public String getPlantForDinner() {
        return plantForDinner;
    }

    public void setPlantForDinner(String plantForDinner) {
        this.plantForDinner = plantForDinner;
    }

    private String plantForDinner;

    public void eatingGrass() {
        System.out.println("Söön rohtu");
    }
}
