package it.vali;

public class Main {

    public static void main(String[] args) {

        LivingPlace livingPlace = new Farm();  // näiteks võid panna siia new Zoo, ja kõik loomad elavad siis Zoo's - kood jääb samaks
        Pig pig = new Pig();
        pig.setName("Kalle");

        livingPlace.addAnimal(pig);

        livingPlace.addAnimal(new WildAnimal());

        System.out.println();

        livingPlace.addAnimal(new Cat());
        livingPlace.addAnimal(new Horse());
        livingPlace.addAnimal(pig);
        livingPlace.addAnimal(pig);

        Cow cow = new Cow();
        livingPlace.addAnimal(cow);

        System.out.println();

        // Tee meetod, mis prindib välja kõik farmis elavad loomad ning mitu neid on

        livingPlace.printAnimalCounts();   // hetkel olengi lisanud ainult kaks siga ja ühe lehma, ehk siis mul on ainult kaks siga farmis hetkel

        // Tee meetod, mis eemaldab farmist looma

        System.out.println();

        livingPlace.removeAnimal("Cow");
        livingPlace.printAnimalCounts();

        System.out.println();

        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();

        System.out.println();

        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();

        System.out.println();

        livingPlace.removeAnimal("Pig");
        livingPlace.printAnimalCounts();

        // Mõelge ja täiendage zoo ja forest klasse nii, et neil oleks nende 3 meetodi sisud

    }
}
