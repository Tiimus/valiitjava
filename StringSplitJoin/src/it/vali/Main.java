package it.vali;

public class Main {

    public static void main(String[] args) {

        //Leiame sõna pikkuse
        String word = "kala";

        int length = word.length();
        System.out.println(length);

        // Kirjutame kõik sõnad massiivis välja
        String[] words = new String[] {"Põdral", "maja", "metsa", "sees"};

        for (int i = 0; i < words.length ; i++) {
            System.out.println(words[i]);
        }

        System.out.println();


        // Prindi kõik sõnad massiivist, mis algavad m-tähega

        for (int i = 0; i < words.length ; i++) {

            String firstLetter = words[i].substring (0,1);

            if (firstLetter.toLowerCase().equals("m")) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Prindi kõik a tähega lõppevad sõnad

        for (int i = 0; i < words.length; i++) {

            length = words[i].length();
            String lastLetter = words[i].substring (length - 1, length);   // length pärast koma sulgudes on optional

            if (lastLetter.toLowerCase().equals("a")) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Loe üle kõik sõnad, mis sisaldavad a tähte

        int wordCounter = 0;

        for (int i = 0; i < words.length ; i++) {

            int aWords = words[i].indexOf("a");  // võib kasutada ka .contains("a")

            if (aWords != -1 ) {
                wordCounter++;
            }
        }
        System.out.println(wordCounter);

        System.out.println();

        // Prindi välja kõik sõnad, kus on 4 tähte

        for (int i = 0; i < words.length; i++) {

            length = words[i].length();

            if (length == 4) {
                System.out.println(words[i]);
            }
        }

        System.out.println();

        // Prindi välja kõige pikem sõna  // teine lahendus Lauri GitLabis

        int max = words[0].length();
        for (int i = 1; i < words.length; i++) {

            if(words[i].length() > max) {
                max = words[i].length();
            }
        }
        // System.out.println(max);    // prindib välja kui palju tähti on pikimas sõnas

        for (int i = 0; i < words.length; i++) {
            int wordsLength = words[i].length();
            if (wordsLength == max) {
                // System.out.println(i);     // prindib välja mitmendal kohal pikim sõna on
                String longestWord = words[i];
                System.out.println(longestWord);   // prindib välja pikima sõna
            }
        }


        System.out.println();

        // Prindi välja sõnad, kus esimene ja viimane täht on sama

        for (int i = 0; i < words.length ; i++) {

            length = words[i].length();
            String firstLetter = words[i].substring (0,1);
            String lastLetter = words[i].substring (length - 1, length);

            if (firstLetter.toLowerCase().equals(lastLetter.toLowerCase())) {
                System.out.println(words[i]);
            }
        }
    }
}
