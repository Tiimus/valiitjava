
package it.vali;

// import tähendab, et antud klassile Main lisatakse ligipääs java class library paketile java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // loome Scanner tüübist objekti nimega scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on sinu nimi?"); // sama mis System.out.print("Tere, mis on sinu nimi?"\n); \n paneb järgmisele reale
        String name = scanner.nextLine();
        System.out.println("Tere " + name + "! Mis on sinu lemmikvärv?");
        String color = scanner.nextLine();
        System.out.println("Väga ilus värv. Aga kuidas on su koera nimi?");
        String dogsName = scanner.nextLine();
            // versioon 1 - kasutab liiga palju mälu
        System.out.println("Väga tore! Teretulemast " + name + " ilus värv on " + color + " ja ilus nimi " + dogsName);
            // ver 2
        System.out.printf("Väga tore! Teretulemast %s ilus värv on %s ja ilus nimi %s \n" , name, color, dogsName);
            // ver 3
        StringBuilder builder = new StringBuilder();
        builder.append("Väga tore! Teretulemast ");
        builder.append(name);
        builder.append(" ilus värv on ");
        builder.append(color);
        builder.append(" ja ilus nimi ");
        builder.append(dogsName);

        System.out.println(builder.toString()); // võib ka String fullText = builder.toString; ja järgmisel real System.out.println(fullText);
            // ver 4
        // Nii System.out.printf kui ka String.format kasutavad enda siseselt StringBuilderit
        String text = String.format("Väga tore! Teretulemast %s ilus värv on %s ja ilus nimi %s \n" , name, color, dogsName); // lihtsalt teha stringi ja mitte välja printida (nätieks vaja hiljem kusagil kasutada)
        System.out.println(text);








    }
}
