package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Flyer bird = new Bird();
        Flyer plane = new Plane();

        bird.fly();

        System.out.println();

        plane.fly();

        List<Flyer> flyers = new ArrayList<Flyer>();
        flyers.add(bird);
        flyers.add(plane);

        Plane boeing = new Plane();
        Bird pigeon = new Bird();

        flyers.add(boeing);
        flyers.add(pigeon);

        System.out.println(boeing);

        System.out.println();

        for (Flyer flyer: flyers) {
            flyer.fly();
            System.out.println();
        }

        Car car = new Car();
        car.stopDriving(30);

        car.setMake("Audi");
        System.out.println(car);  // põhimõtteliselt println kutsub välja toStringi meetodi

        // Lisa liides Driver, klass Car.
        // Mõtle kas lennuk ja auto võiks mõlemat kasutada Driver liidest?
        // Driver liides võiks sisaldada 3 meetodi kirjeldust:
        // int getMaxDistance();
        // void drive();
        // void stopDriving(int afterDistance); - saad määrata mitme meetri pärast lõpetab
        // Pane auto ja lennuk mõlemad kasutama seda liidest
        // Lisa mootorratas
    }
}
