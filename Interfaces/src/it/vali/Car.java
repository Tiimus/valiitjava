package it.vali;

public class Car implements Driver {

    private int maxDistance;
    private String make;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    // konstruktor
    public Car() {
        this.maxDistance = 600;
    }

    @Override
    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    public void drive() {
        System.out.println("Auto sõidab");
    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Auto lõpetab sõitmise %d km pärast%n", afterDistance);
    }


    // Code > Override methods
    @Override
    public String toString() { //println'iga prindib välja objektiks, mida sa tahad välja printida(võib ka mida iganes sinna panna) // car.setMake("Audi");
                                                                                                                                   // System.out.println(car);
        return make;
    }
}
