package it.vali;

public class Bike extends Vehicle implements DriverInTwoWheels {   // ei pane Driver'it juurde, sest DriverInTwoWheels extends Driver
    @Override
    public int getMaxDistance() {
        return 300;
    }

    @Override
    public void drive() {
        System.out.println("Mootorratas sõidab");

    }

    @Override
    public void stopDriving(int afterDistance) {
        System.out.printf("Mootorratas lõpetab sõitmise %d km pärast%n", afterDistance);
    }

    @Override
    public void driveInRearWheel() {
        System.out.println("Mootorratas sõidab tagarattal");
    }
}
