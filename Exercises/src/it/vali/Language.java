package it.vali;

import java.util.List;


public class Language {

    // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega, kus seda keelt räägitakse.
    // Kirjuta üle selle klassi meetod toString(); nii, et see tagastab riikide nimekirja eraldades komaga.
    // Tekita antud klassist üks objekt ühe vabalt valitud keele andmetega ning prindi välja selle objekti toString() meetodi sisu.

    private String languageName;
    private List<String> countryNames;

    public List<String> getCountryNames() {
        return countryNames;
    }

    public void setCountryNames(List<String> countryName) {
        this.countryNames = countryName;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    @Override
    public String toString() {
        return String.join(", ", countryNames);
    }
}
