package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // 1. Arvuta ringi pindala, kui teada on raadius, prindi pindala välja ekraanile

        double radius = 5;
        double area = Math.PI * (Math.pow(radius, 2));

        System.out.printf("Ringi pindala on %.2f%n", area);

        System.out.printf("Ringi pindala on %.2f%n", calulateCirleArea(5));

        // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriteks on kaks stringi
        //  Meetod tagastab, kas tõene või vale selle kohta, kas stringid on võrdsed

        System.out.printf("Kas string %s ja string %s on võrdsed: %s%n", "tere", "head aega", areStringsEqual("tere", "head aega"));
        System.out.println();
        System.out.printf("Kas string %s ja string %s on võrdsed: %s%n", "tere", "tere", areStringsEqual("tere", "tere"));

        // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi.
        // Iga sisend massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
        // 3, 6, 7
        // "aaa", "aaaaaa" etc

        System.out.println();

        int[] intArray = new int[] {3, 4, 7};
        String[] stringArray = numbersToA(intArray);
        for (int i = 0; i < stringArray.length; i++) {
            System.out.println(stringArray[i]);
        }


        // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
        // (sisestada saab ainult aastaid vahemikus 500-2019)
        // ja tagastab kõik sellel sajandil esinenud liigaastad.
        // Ütle veateade, kui aastaarv ei mahu vahemikku



        for (int year: leapYearsInCentury(400)) {
            System.out.println(year);
        }



        // 5. Defineeri klass Language, sellel klassil getLanguageName, setLanguageName ning list riikide nimedega, kus seda keelt räägitakse.
        // Kirjuta üle selle klassi meetod toString(); nii, et see tagastab riikide nimekirja eraldades komaga.
        // Tekita antud klassist üks objekt ühe vabalt valitud keele andmetega ning prindi välja selle objekti toString() meetodi sisu.

        Language language = new Language();
        language.setLanguageName("English");

        List<String> countryNames = new ArrayList<String>();
        countryNames.add("USA");
        countryNames.add("UK");
        countryNames.add("Australia");

        language.setCountryNames(countryNames);

        System.out.println(language.toString()); // võib ka toString()'i ära jätta, ilma töötab ka

    }

    // 1. Arvuta ringi pindala, kui teada on raadius, prindi pindala välja ekraanile
    static double calulateCirleArea(double radius) {

        double area = Math.PI * (Math.pow(radius, 2));   // võib ka kokku panna: return = Math.PI * (Math.pow(radius, 2));
        return area;
    }

    // 2. Kirjuta meetod, mis tagastab boolean tüüpi väärtuse ja mille sisendparameetriteks on kaks stringi
    //  Meetod tagastab, kas tõene või vale selle kohta, kas stringid on võrdsed

    static boolean areStringsEqual(String firstString, String secondString) {

        if (firstString.equals(secondString)) {   // return firstString.equals(secondString);
            return true;
        }
        return false;  // kui on if, returnis pole else'i vaja

    }

    // 3. Kirjuta meetod, mille sisendparameetriks on täisarvude massiiv ja mis tagastab stringide massiivi.
    // Iga sisend massiivi elemendi kohta olgu tagastatavas massiivis samapalju a tähti.
    // 3, 6, 7
    // "aaa", "aaaaaa" etc

    static String[] numbersToA(int[] intArray) {
        String[] stringArray = new String[intArray.length];
        for (int i = 0; i < intArray.length; i++) {
            stringArray[i] = generateAString(intArray[i]);
        }
        return stringArray;
    }

    static String generateAString(int count) {
        String word = "";

        for (int i = 0; i < count; i++) {
            word += "a";
        }
        return word;
    }

    // 4. Kirjuta meetod, mis võtab sisendparameetrina aastaarvu
    // (sisestada saab ainult aastaid vahemikus 500-2019)
    // ja tagastab kõik sellel sajandil esinenud liigaastad.
    // Ütle veateade, kui aastaarv ei mahu vahemikku

    static List<Integer> leapYearsInCentury(int year) {


        if(year < 500 || year > 2019) {
            System.out.println("Aasta peab olema 500 ja 2019 vahel");
            return new ArrayList<Integer>();
        }
        int centuryStart = year / 100 * 100;
        int centuryEnd = centuryStart + 99;
        int leapYear = 2020;

        List<Integer> years = new ArrayList<Integer>();

        for (int i = 2020; i >= centuryStart; i-=4) {
            if(i <= centuryEnd && i % 4 == 0) {
                years.add(i);
            }
        }

        return years;
    }

}
