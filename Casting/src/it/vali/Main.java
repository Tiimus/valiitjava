package it.vali;

public class Main {

    public static void main(String[] args) {
	// Casting on teisendamine ühest arvutüübist teise
        byte a = 3;

        // Kui üks numbritüüp mahub teise sisse, siis toimub automaatne teisendamine - implicit casting
        short b = a;

        // kui üks numbritüüp ei pruugi mahtuda teise sisse, siis peab kõigepealt ise veenduma, et see number mahub sinna teise numbritüüpi
        // ja kui ei mahu, siis peab ise seda teisendama - explicit casting

        short c = 128;
        byte d = (byte)c;

        System.out.println(d); // prindib -128, range hakkab ringiga uuesti peale: byte range -128 kuni 127, kui panna 128 väärtuseks, siis on üks suurem kui 127, nii et algab range'i algusest jälle peale ehk siis -128

        long e = 10000000000L;
        int f = (int) e;

        System.out.println(f);

        long g = f;
        g = c;
        g=d;

        // mahub üksteise sisse
        float h = 123.23424f;
        double i = h;

        //  float j = i; ei mahu


        double j = 55.111111111;

        // Antud juhul toimub ümardamine
        float k = (float)j;
        System.out.println(k);

        double l = 12E50;  // 12 * 10 astmel 50
        float m = (float)l;
        System.out.println(m);  // prints out Infinity - ei mahu floati (?)

        int n = 3;
        double o = n; // mahub sisse, iga naturaalarv on ka reaalarv

        double p = 2.99;
        short q = (short) p;

        System.out.println(q); // kõik peale koma jäetakse ära

       int r = 2;
       double s = 9;

       System.out.println(r / s);

        int ra = 2;
        int sa = 9;
        // int / int = int
        // int / double = int
        // double / int = double
        // double + int = double
        // double / float = double

        System.out.println((double)ra / sa);

        System.out.println((float)ra / sa);

        float t = 12.55555F;
        double u = 23.555555;

        System.out.println(t / u);











    }
}
