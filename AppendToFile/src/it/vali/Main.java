package it.vali;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try {
            FileWriter fileWriter = new FileWriter ("output.txt");
            fileWriter.append("Tere\r\n");

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }



        // Koosta täisarvude massiiv 10st arvust ning seejärel kirjuta faili kõik suuremad arvud kui 2.

        try {
            FileWriter fileWriter = new FileWriter("numbers.txt");
            int[] numbers = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

            for (int i = 0; i < numbers.length; i++) {
                if (numbers[i] > 2) {
                    System.out.println(numbers[i]);

                    fileWriter.write(numbers[i] + System.lineSeparator());
                }
            }
            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }


        // Küsi kasutajalt 2 arvu. Küsi seni kuni mõlemad on korrektses formaadis (numbrid) ning nende summa ei ole paarisarv.

        Scanner scanner = new Scanner(System.in);


        int a = 0;
        int b = 0;

        do {
            boolean correctNumber = false;
            do {
                System.out.println("Palun kirjuta number");
                try {
                    a = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Number oli vigane");
                }
            } while (!correctNumber);

            correctNumber = false;

            do {
                System.out.println("Palun kirjuta veel üks number");
                try {
                    b = Integer.parseInt(scanner.nextLine());
                    correctNumber = true;
                } catch (NumberFormatException e) {
                    System.out.println("Number oli vigane");
                }
            } while (!correctNumber);

        } while ((a + b) % 2 == 0);

        // Küsi kasutajalt kõigepealt mitu arvu ta tahab sisestada, seejärel küsi kasutajalt ühekaupa need arvud ning
        // kirjuta nende arvude summa faili, nii et see lisatakse alati faili (Append)
        // Tulemus on, et iga programmi käivitamisel on failis kõik eelnevad summad kirjas.

        int sum = 0;
        System.out.println("Mitu numbrit sa soovid sisestada?");
        int howManyTimes = 0;
        try {
            howManyTimes = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("Number oli vigane");
        }
        do {
            System.out.println("Palun sisesta arv");
            int answer = Integer.parseInt(scanner.nextLine());
            howManyTimes--;
            sum = sum + answer;
        } while (howManyTimes != 0);

        System.out.println("Arvude summa on: " + sum);

        try {
            FileWriter fileWriter = new FileWriter ("addedNumbers.txt", true);
            fileWriter.append(sum + System.lineSeparator());

            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }

        // teine variant

        System.out.println("Mitu arvu soovid sisestada?");
        int count = Integer.parseInt(scanner.nextLine());

        sum = 0;

        for (int i = 0; i < count; i++) {
            System.out.printf("Sisesta arv number %d", i + 1);
            int number = Integer.parseInt(scanner.nextLine());
            sum += number;
        }
        try {
            FileWriter fileWriter = new FileWriter ("addedNumbers.txt", true);
            fileWriter.append(sum + System.lineSeparator());

            fileWriter.close();

        } catch (IOException e) {
            System.out.println("Viga: antud failile ligipääs ei ole võimalik");
        }
    }
}
