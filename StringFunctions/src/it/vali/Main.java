package it.vali;

public class Main {

    public static void main(String[] args) {
	    String sentence = "KElas metsas Mutionu keset Kuuski noori vanu";

        // Sümbolite indeksid tekstis algavad samamoodi indeksiga 0 nagu massiivides.

	    // Leia üles esimene tühik, mis on tema indeks

        int spaceIndex = sentence.indexOf(" ");
        // indexOf tagastab -1, kui otsitavat fraasi (sümbolit või sõna) ei leitud ning indeksi (kust sõna algab) kui fraas leitakse
        // kui rohkem kui üks character, siis leiab selle koha kust see sõna algab

        // Leia teine tühik
        // Prindi välja kõigi tühikute indeksid lauses
        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);  // .indexOf(mida ma otsin, millest alates)

        }

        System.out.println();

        // Prindi taganpoolt kõik tühikud välja

        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);

        }

        // Prindi lause esimesed 4 tähte

        // .substring() => Returns a string that is a substring of this string ("Elast metsas..").
        // .substring(beginIndex, endIndex - 1) => beginIndex - the beginning index, inclusive; endIndex - the ending index, exclusive.
        // Thus the length of the substring is {endIndex-beginIndex}.
        // All the info when you right click on substring > Go To > Declaration

        String part = sentence.substring(0, 4);
        System.out.println(part);

        // kirjuta esimene sõna lauses
        part = sentence.substring(0, sentence.indexOf(" "));
        System.out.println(part);

        // kirjuta teine sõna

        spaceIndex = sentence.indexOf(" ");
        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);

        // miks üleval +1 tähendab järgmist tühikut, aga järgmises reas tähendab see järgmist indexit?

        String secondWord = sentence.substring(spaceIndex + 1, secondSpaceIndex);
        System.out.println(secondWord);

        // Leia esimene k tähega algav sõna
        // et leiaks ka siis, kui see sõna on lause esimene sõna


        String firstLetter = sentence.substring(0, 1);
        String kWord = "";    //mida "" siin tähendab?

        if (firstLetter.toLowerCase().equals("k")) {
            spaceIndex = sentence.indexOf(" ");
            kWord = sentence.substring (0, spaceIndex);
            System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
        }
        else {
            sentence = sentence.toLowerCase();
            int kIndex = sentence.indexOf(" k") + 1;
            if(kIndex != 0) {

                spaceIndex = sentence.indexOf(" ", kIndex); // indexOf( mida otsib (" "), millest alates (kIndex) )

                kWord = sentence.substring(kIndex, spaceIndex);

            }

         if (kWord.equals("")) {
             System.out.println("Puudub k-tähega sõna");
         }
         else {
             System.out.printf("Esimene k-tähega algav sõna on %s%n", kWord);
         }

        }


        // Leia mitu sõna lauses on
        int spaceCounter = 0;
        spaceIndex = sentence.indexOf(" ");
        while (spaceIndex != -1) {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1);
            spaceCounter++;
        }

        System.out.printf("Sõnade arv lauses on %d%n", spaceCounter + 1);

        // Leia mitu k tähega algavat sõna on

        int kWordCounter = 0;
        sentence = sentence.toLowerCase();
        int kWordIndex = sentence.indexOf(" k");

        if (sentence.substring(0,1).equals("k")) {   //  võiks ka teha nii  =>  if (firstLetter.toLowerCase().equals("k")) {
            kWordCounter++;
        }
        while (kWordIndex != -1) {
            kWordIndex = sentence.indexOf(" k", kWordIndex + 1);
            kWordCounter++;
        }
        System.out.printf("K-tähega algavaid sõnu on %d%n", kWordCounter);

    }
}
