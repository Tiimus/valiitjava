package it.vali;

public class Main {

    public static void main(String[] args) {
	    int a = 21;

	    // Kui arv on 4, siis prindi arv on 4
        // muul juhul kui arv on negatiivne, siis kontrolli kas arv on suurem kui -10, prindi sellekohane tekst
        //muul juhul kontrolli kas arv on suurem kui 20 ja prindi sellekohane tekst
	    if (a == 4) {
	        System.out.println("Arv on 4");
        }
	    else {
	        if (a < 0 && a > -10) {
                System.out.println("Arv on negatiivne, kuid mitte suurem kui -10");
            }
	        else {
	            if (a > 20) {
                    System.out.println("Arv on suurem kui 20");
                }
            }
        }
    }
}
