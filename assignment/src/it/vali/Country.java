package it.vali;

import java.util.List;

public class Country {
    private int Population;
    private String name;
    private List<String> mostPopularLanguages;

    public int getPopulation() {
        return Population;
    }

    public void setPopulation(int population) {
        Population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMostPopularLanguages() {
        return mostPopularLanguages;
    }

    public void setMostPopularLanguages(List<String> mostPopularLanguages) {
        this.mostPopularLanguages = mostPopularLanguages;
    }

    @Override
    public String toString() {
        return String.format("%s:%nPopulation: %d%nMost popular languages: %s", getName(), getPopulation(), getMostPopularLanguages());
    }

}
