package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    // Ülesanne 1
        double pi = Math.PI;
        pi *= 2;
        System.out.println(pi);

        System.out.println();

        // Ülesanne 2

        int firstNumber = 2;
        int secondNumber = 3;
        System.out.printf("Kas number %d ja number %d on võrdsed: %s%n", firstNumber, secondNumber, areEqual(2, 3) );

        System.out.println();

        // Ülesanne 3

        String[] words  = new String[] {"Kass", "Ema", "Kapsas"};
        int [] wordsLength = howLongString(words);
        for (int i = 0; i < words.length; i++) {
            System.out.println(wordsLength[i]);
        }

        System.out.println();
        // Ülesanne 4

        // System.out.println(whatCentury(1));

        int [] years = new int[] {0, 1, 128, 598, 1624, 1827, 1996, 2017};

        for (int i = 0; i < years.length; i++) {
            years[i] = whatCentury(years[i]);
            System.out.println(years[i]);
        }


        // Ülesanne 5

        Country country = new Country();
        country.setName("Eesti");
        country.setPopulation(1300000);

        List<String> mostPopularLanguages = new ArrayList<String>();
        mostPopularLanguages.add("eesti keel");
        mostPopularLanguages.add("vene keel");
        mostPopularLanguages.add("inglise keel");

        country.setMostPopularLanguages(mostPopularLanguages);

        System.out.println(country.toString());

    }

    // Ülesanne 2

    static boolean areEqual (int firstNumber, int secondNumber) {
        if(firstNumber == secondNumber) {
            return true;
        }
        return false;
    }

    // Ülesanne 3

    static int[] howLongString (String[] stringArray) {

        int[] stringLength = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            stringLength[i] = howLong(stringArray[i]);
        }
        return stringLength;
    }

    static int howLong (String word) {

        int howLong = 0;

        for (int i = 0; i < word.length(); i++) {
            howLong++;
        }
        return howLong;
    }

    // Ülesanne 4

    static byte whatCentury(int year) {
        if(year < 1 || year > 2018) {
            return -1;
        }
        byte century = 0;
        for (int i = 0; i < 2018 ; i++) {
            year = year / 100 + 1;
            return (byte) year;
        }
        return century;
    }

}
